#pragma once


typedef int(*GetDeviceStatusHW)(int nlabel, LPWSTR sHwid);
typedef int(*GetDeviceStatusSymbl)(int nlabel, LPWSTR symbl, int nPort);
typedef int(*GetDeviceLabelPort)(int nlabel, LPWSTR sHwid, LPWSTR sFileName);
typedef int(*GetSemaphoreCurCnt)(LPWSTR sSemaphoreName);
typedef int(*GetDeviceInstanceIdFromSymbl)(
	int nlabel,
	LPCWSTR symbl,
	int nPort,
	LPWSTR lpReturnedString,
	DWORD nSize
	);
typedef int(*GetDeviceSymblFromInstanceId)(
	int nlabel,
	LPCWSTR sInstanceId,
	LPWSTR lpReturnedString,
	DWORD nSize
	);
typedef int (*GetDeviceSymblFromInstanceIdEx)(
	int nlabel,
	LPCWSTR sInstanceId,
	LPWSTR lpReturnedString,
	DWORD nSize,
	GUID  interfaceguid
	);

typedef int (*GetCommonSymbl)(int nlabel, LPCTSTR sHubName, int nPort, PDEVPROPKEY pdevkey, PBYTE nRet, int &nLen, DWORD &nType);
typedef int (*GetCommonHW)(int nlabel, LPCTSTR sHwid, PDEVPROPKEY pdevkey, PBYTE nRet, int &nLen, DWORD &nType);

typedef int (*GetChildPropertyFromHubPort)(
	_In_	int nlabel,
	_In_     LPCWSTR sHubName,
	_In_	 int nPort,
	_In_	 PDEVPROPKEY pdevkey,
	_Out_    PBYTE nRet,
	_Inout_  int &nLen,
	_Out_    DWORD &nType
	);


class CDeviceStatus
{
public:
	CDeviceStatus();
	~CDeviceStatus();

	int InitDll();
	void FreeDll();
	int procGetDeviceStatusHW(int nlabel, LPTSTR sHwid);
	int procGetDeviceStatusSymbl(int nlabel, LPTSTR symbl, int nPort);
	int procGetDeviceLabelPort(int nlabel, LPTSTR sHwid, LPTSTR sFileName);
	int procGetSemaphoreCurCnt(LPTSTR sSemaphoreName);
	int procGetDeviceInstanceIdFromSymbl(
		int nlabel,
		LPCTSTR symbl,
		int nPort,
		LPTSTR lpReturnedString,
		DWORD nSize
		);
	int  procGetDeviceSymblFromInstanceId(
		int nlabel,
		LPCTSTR sInstanceId,
		LPTSTR lpReturnedString,
		DWORD nSize
		);

	int procGetCommonSymbl(int nlabel, LPCTSTR sHubName, int nPort, PDEVPROPKEY pdevkey, PBYTE nRet, int &nLen, DWORD &nType);
	int procGetCommonHW(int nlabel, LPCTSTR sHwid, PDEVPROPKEY pdevkey, PBYTE nRet, int &nLen, DWORD &nType);
	int procGetChildPropertyFromHubPort(
		_In_	 int nlabel,
		_In_     LPCWSTR sHubName,
		_In_	 int nPort,
		_In_	 PDEVPROPKEY pdevkey,
		_Out_    PBYTE nRet,
		_Inout_  int &nLen,
		_Out_    DWORD &nType
		);


private:
#define declare_entry(f)        f fn_##f
	declare_entry(GetDeviceStatusHW);
	declare_entry(GetDeviceStatusSymbl);
	declare_entry(GetDeviceLabelPort);
	declare_entry(GetSemaphoreCurCnt);
	declare_entry(GetDeviceInstanceIdFromSymbl);
	declare_entry(GetDeviceSymblFromInstanceId);
	declare_entry(GetDeviceSymblFromInstanceIdEx);
	declare_entry(GetCommonSymbl);
	declare_entry(GetCommonHW);
	declare_entry(GetChildPropertyFromHubPort);
#undef declare_entry

	HMODULE	m_hDll;

};

