#pragma once
// HDEVINFO  BeginEnumeratePorts(VOID);
//BOOL EnumeratePortsNext(HANDLE DeviceInfoSet, LPTSTR lpBuffer, int nLen, LPCTSTR szParenLocationPath);
//BOOL  EndEnumeratePorts(HANDLE DeviceInfoSet);
//DWORD GetHubPortLocationpath(TCHAR *sHubName, int nPort, TCHAR *sInstanceId, DWORD nLen);

//DWORD GetDeviceRead(TCHAR *HubName, int nPort);
DWORD GetDeviceInfo(TCHAR *HubName, int nPort, BOOL bGetInfo, BOOL bGetActiveLock=TRUE);
DWORD GetDeviceInfoLG(TCHAR *HubName, int nPort, BOOL bGetInfo, BOOL bGetActiveLock = TRUE);
DWORD DoRefurbish(TCHAR *sHubName, int nPort);
DWORD DoRefurbishLG(TCHAR *sHubName, int nPort);
DWORD DoReactivation(TCHAR *sHubName, int nPort);
DWORD CheckRefurbishReboot(TCHAR *sHubName, int nPort, int nTimeout);
DWORD CheckConnectDevice(TCHAR *sHubName, int nPort, int nPid, int nVid);
DWORD CheckCDROMEject(TCHAR *sHubName, int nPort);

void logIt(TCHAR* fmt, ...);
void logHex(BYTE* buffer, size_t buf_sz, TCHAR* fmt, ...);

VOID
Oops
(
__in PCTSTR File,
ULONG Line,
DWORD dwError
);

#define OOPS()		Oops(_T(__FILE__), __LINE__, GetLastError())
#define OOPSERR(d)	Oops(_T(__FILE__), __LINE__, d)
#define ENTER_FUNCTION()	logIt(_T("%s ++\n"), _T(__FUNCTION__))
#define EXIT_FUNCTION()		logIt(_T("%s --\n"), _T(__FUNCTION__))
#define EXIT_FUNCTRET(ret)		logIt(_T("%s -- return=%d\n"), _T(__FUNCTION__), ret)


int  QueryRecordFrp(CString model, CString ver);
void InsertDBData(CString model, CString ver);

void DevconINFOResponse(char* data, std::map<CString, CString> &info);
void ATIINFOResponse(char* data, std::map<CString, CString> &info);

CString LookForBlackList(std::set<CString> vers, CString spModel, CString sMaker=_T("Samsung"));
