#include "stdafx.h"
#define INITGUID
#include <initguid.h>
#include <Devpropdef.h>
#include <Devpkey.h>
#include <stdlib.h>
#include <Usbioctl.h>
#include <set>
#include <regex>
#include <string>
#include <vector>

#include <algorithm>

#include "commoncheader.h"
#include "DeviceStatus.h"

#include <msclr\marshal.h>
#include <msclr\marshal_cppstd.h>
#include <vcclr.h>


#define MAX_NAME_PORTS 7
#define RegDisposition_OpenExisting (0x00000001) // open key only if exists
#define CM_REGISTRY_HARDWARE        (0x00000000)

BOOL CheckDeviceModem(TCHAR *sHubName, int nPort, int nPid, int nVid);

extern int glabel;
extern int gTimeOut;

BOOL gIngoreWaitReboot = false;
BOOL gWaitDeviceGone = true;
extern int gLowTimeOut;
//#define IOCTL_USB_HUB_CYCLE_PORT  \
//                                CTL_CODE(FILE_DEVICE_USB,  \
//                                USB_HUB_CYCLE_PORT,  \
//                                METHOD_BUFFERED,  \
//                                FILE_ANY_ACCESS)
//
//void Hub_PlugOff(LPCTSTR hubName, DWORD PortIndex)
//{
//	SECURITY_ATTRIBUTES SA = { 0 };
//	SA.nLength = sizeof(SECURITY_ATTRIBUTES);
//	HANDLE HubHandle = CreateFile(hubName, GENERIC_WRITE, FILE_SHARE_WRITE, &SA, OPEN_EXISTING, 0, NULL);
//	if (HubHandle != INVALID_HANDLE_VALUE)
//	{
//		DWORD BytesReturned = 0;
//		typedef struct _USB_CYCLE_PORT_PARAMS {
//			ULONG ConnectionIndex;
//			ULONG StatusReturned;
//		} USB_CYCLE_PORT_PARAMS, *PUSB_CYCLE_PORT_PARAMS;
//		USB_CYCLE_PORT_PARAMS ConnectedHub = { PortIndex, 0 };
//		if (!DeviceIoControl(HubHandle, IOCTL_USB_HUB_CYCLE_PORT, &ConnectedHub, sizeof(ConnectedHub), &ConnectedHub, sizeof(ConnectedHub), &BytesReturned, NULL))
//		{
//			OOPS(); /// getlasterror 50
//		}
//		CloseHandle(HubHandle);
//	}
//}

const char hexmap[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

void QueryBlackList();

std::string hexStr(unsigned char *data, int len)
{
	std::string s(len * 2, ' ');
	for (int i = 0; i < len; ++i) {
		s[2 * i] = hexmap[(data[i] & 0xF0) >> 4];
		s[2 * i + 1] = hexmap[data[i] & 0x0F];
	}
	return s;
}

std::wstring s2ws(const std::string& s)
{
	int len;
	int slength = (int)s.length() + 1;
	len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0);
	wchar_t* buf = new wchar_t[len];
	MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
	std::wstring r(buf);
	delete[] buf;
	return r;
}

bool startsWith(const TCHAR *pre, const TCHAR *str)
{
	size_t lenpre = _tcslen(pre),
		lenstr = _tcslen(str);
	return lenstr < lenpre ? false : _tcsncmp(pre, str, lenpre) == 0;
}

#include <map>
#include <list>
#include "SerialPort.h"
std::map<CString, CString> m_info;
BOOL IsNoConnected(TCHAR *sHubName, int nPort, CString& sDriverKey);

DWORD GetHubPortLocationpath(TCHAR *sHubName, int nPort,TCHAR *sInstanceId, DWORD nLen)
{
	ENTER_FUNCTION();
	TCHAR symblName[1024] = { 0 };
	int ret = ERROR_UNKNOWN_FEATURE;
	if (sHubName == NULL || _tcslen(sHubName) == 0 || nLen == 0 || sInstanceId ==NULL)
		return ERROR_INVALID_PARAMETER;

	if ((_tcsncicmp(sHubName, _T("\\\\?\\"), 4) == 0) || (_tcsncicmp(sHubName, _T("\\\\.\\"), 4) == 0))
	{
		_stprintf_s(symblName, _T("%s"), sHubName);
	}
	else
	{
		_stprintf_s(symblName, _T("%s%s"), _T("\\\\?\\"), sHubName);
	}

	HDEVINFO hDevInfo = SetupDiCreateDeviceInfoList(NULL, NULL);
	if (hDevInfo != INVALID_HANDLE_VALUE)
	{
		DWORD sz = 0;
		SP_DEVICE_INTERFACE_DATA devIntData;
		devIntData.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);
		if (SetupDiOpenDeviceInterface(hDevInfo, symblName, 0, &devIntData))
		{
			DEVPROPTYPE type;
			BYTE b[2048];
			ZeroMemory(b, sizeof(b));
			sz = 0;

			SP_DEVINFO_DATA devInfoData = { 0 };
			devInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
			SetupDiGetDeviceInterfaceDetail(hDevInfo, &devIntData, NULL, 0, &sz, &devInfoData);
			if (GetLastError() == ERROR_INSUFFICIENT_BUFFER)
			{
				if (SetupDiGetDeviceProperty(hDevInfo, &devInfoData, &DEVPKEY_Device_LocationPaths, &type, b, 2048, &sz, 0))
				{
					//logIt(_T("Property: #%d\n"), i + 1);
					//dump(p, type, b, sz);
					if (type == DEVPROP_TYPE_STRING_LIST)
					{
						TCHAR *instanceid = (TCHAR *)b;
						_stprintf_s(sInstanceId, nLen, _T("%s#USB(%d)"), instanceid, nPort);
						logIt(_T("Hub Location Paths : %s\n"), sInstanceId);
					}
				}
				else
				{
					ret = GetLastError();
					OOPSERR(ret);
				}
			}
			else
			{
				ret = GetLastError();
				OOPSERR(ret);
			}
		}
		else
		{
			ret = GetLastError();
			OOPSERR(ret);
		}

		SetupDiDestroyDeviceInfoList(hDevInfo);
	}
	else
	{
		ret = GetLastError();
		OOPSERR(ret);
	}
	EXIT_FUNCTRET(ret);
	return ret;

}


HDEVINFO  BeginEnumeratePorts(VOID)
{
	ENTER_FUNCTION();
	BOOL guidTest = FALSE;
	DWORD RequiredSize = 0;
	HDEVINFO DeviceInfoSet;
	LPGUID buf = NULL;

	guidTest = SetupDiClassGuidsFromName(
		_T("Modem"), 0, 0, &RequiredSize);
	if (RequiredSize < 1)return INVALID_HANDLE_VALUE;

	buf = (LPGUID)malloc(RequiredSize*sizeof(GUID));

	guidTest = SetupDiClassGuidsFromName(
		_T("Modem"), buf, RequiredSize*sizeof(GUID), &RequiredSize);

	if (!guidTest)
	{
		if (buf != NULL) free(buf);
		return INVALID_HANDLE_VALUE;
	}


	DeviceInfoSet = SetupDiGetClassDevs(buf, NULL, NULL, DIGCF_PRESENT);

	if (buf != NULL) free(buf);

	return DeviceInfoSet;
}

BOOL EnumeratePortsNext(HDEVINFO DeviceInfoSet, LPTSTR lpBuffer, int nLen, LPCTSTR szParenLocationPath)
{
	ENTER_FUNCTION();
	if (DeviceInfoSet == NULL) return false;
	BOOL bFind = false;
	DWORD dindex = 0;
	SP_DEVINFO_DATA DeviceInfoData = { 0 };
	DeviceInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
	for (; SetupDiEnumDeviceInfo(DeviceInfoSet, dindex, &DeviceInfoData); dindex++)
	{
		DEVPROPTYPE type;
		BYTE b[2048];
		ZeroMemory(b, sizeof(b));
		DWORD sz = 0;

		if (SetupDiGetDeviceProperty(DeviceInfoSet, &DeviceInfoData, &DEVPKEY_Device_LocationPaths, &type, b, 2048, &sz, 0))
		{
			if (type == DEVPROP_TYPE_STRING_LIST)
			{
				TCHAR *instanceid = (TCHAR *)b;
				TCHAR sInstanceId[MAX_PATH] = { 0 };
				_stprintf_s(sInstanceId, _T("%s"), instanceid);
				logIt(_T("DEV Location Paths : %s\n"), sInstanceId);
				if (startsWith(szParenLocationPath, sInstanceId))
				{
					if (SetupDiGetDeviceProperty(DeviceInfoSet, &DeviceInfoData, &DEVPKEY_Device_InstanceId, &type, b, 2048, &sz, 0))
					{
						using namespace System;
						using namespace Microsoft::Win32;
						RegistryKey^ rk;
						String^ spath = String::Format("SYSTEM\\CurrentControlSet\\Enum\\{0}\\Device Parameters", gcnew String((TCHAR*)b));//HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Enum\USB\VID_04E8&PID_6860&Modem\6&dc9ff83&0&0001\Device Parameters
						rk = Registry::LocalMachine->OpenSubKey(spath, false);
						String^ sPort = (String^)(rk->GetValue("PortName"));
						if (!String::IsNullOrEmpty(sPort))
						{
							pin_ptr<const TCHAR> content = PtrToStringChars(sPort);
							_stprintf_s((TCHAR *)lpBuffer, nLen, _T("%s"), content);
							logIt(_T("port found %s.\n"), (TCHAR* )lpBuffer);
							bFind = TRUE;
						}
					}
					else
					{
						OOPS();
					}

					break;
				}
			}

		}
	}
	return bFind;
}

DWORD GetModemFromHub(TCHAR* hubName, int nport)
{
	ENTER_FUNCTION();
	int ret = ERROR_INVALID_PARAMETER;
	CString sDriverKey;
	TCHAR symblName[1024] = { 0 };
	if (hubName == NULL || _tcslen(hubName) == 0)
		return ERROR_INVALID_PARAMETER;

	if ((_tcsncicmp(hubName, _T("\\\\?\\"), 4) == 0) || (_tcsncicmp(hubName, _T("\\\\.\\"), 4) == 0))
	{
		_stprintf_s(symblName, _T("%s"), hubName);
	}
	else
	{
		_stprintf_s(symblName, _T("%s%s"), _T("\\\\?\\"), hubName);
	}

	IsNoConnected(symblName, nport, sDriverKey);
	if (sDriverKey.GetLength() == 0)
	{
		OOPS();
		return 1;
	}

	GUID* guid = (GUID*)(void*)&GUID_DEVINTERFACE_USB_DEVICE; //Samsung is CDROM when phone connect pc
	// Get device interface info set handle for all devices attached to system
	HDEVINFO hDevInfo = SetupDiGetClassDevs(guid, NULL, NULL,
		DIGCF_PRESENT | DIGCF_DEVICEINTERFACE);
	if (hDevInfo == INVALID_HANDLE_VALUE) {
		ret = GetLastError();
		OOPSERR(ret);
		return ret;
	}
	std::list<CString> listchildrenDevs;
	SP_DEVINFO_DATA spdevinfodata;
	spdevinfodata.cbSize = sizeof(SP_DEVINFO_DATA);
	for (DWORD ndex = 0; SetupDiEnumDeviceInfo(hDevInfo, ndex++, &spdevinfodata);)
	{
		DEVPROPTYPE type;
		BYTE b[4096] = { 0 };
		ZeroMemory(b, sizeof(b));
		DWORD sz = sizeof(b);

		if (SetupDiGetDeviceProperty(hDevInfo, &spdevinfodata, &DEVPKEY_Device_Driver, &type, b, sz, &sz, 0))
		{
			if (sDriverKey.CompareNoCase((TCHAR*)b) == 0)
			{
				ZeroMemory(b, sizeof(b));
				sz = sizeof(b);
				if (SetupDiGetDeviceProperty(hDevInfo, &spdevinfodata, &DEVPKEY_Device_InstanceId, &type, b, sz, &sz, 0))
				{
					CString sInstanceID = (TCHAR *)b;
					ZeroMemory(b, sizeof(b));
					sz = sizeof(b);
					//get children
					if (SetupDiGetDeviceProperty(hDevInfo, &spdevinfodata, &DEVPKEY_Device_Children, &type, b, sz, &sz, 0))
					{
						TCHAR *cmpids = (TCHAR *)b;
						DWORD dwoffset = 0;
						while (cmpids != NULL && _tcslen(cmpids)>0)
						{
							logIt(cmpids);
							listchildrenDevs.push_back(cmpids);
							dwoffset += _tcslen(cmpids) + 1;
							cmpids = (TCHAR *)b + dwoffset;
						}
						break;
					}
					else
					{
						ret = GetLastError();
						OOPSERR(ret);
					}

				}
				else
				{
					ret = GetLastError();
					OOPSERR(ret);
				}

			}
		}
		else
		{
			ret = GetLastError();
			OOPSERR(ret);
		}

	}
	SetupDiDestroyDeviceInfoList(hDevInfo);

	if (listchildrenDevs.size() > 0)
	{
		CDeviceStatus devstatus;
		devstatus.InitDll();
		DEVPROPTYPE type;
		BYTE b[4096] = { 0 };
		ZeroMemory(b, sizeof(b));
		int sz = sizeof(b);
		BOOL bFind = false;
		for each (CString item in listchildrenDevs)
		{
			sz = sizeof(b);
			if ((ret=devstatus.procGetCommonHW(glabel, item, (PDEVPROPKEY)&DEVPKEY_Device_CompatibleIds, b, sz, type)) == ERROR_SUCCESS)
			{
				TCHAR *cmpids = (TCHAR *)b;
				DWORD dwoffset = 0;
				while (cmpids != NULL && _tcslen(cmpids)>0)
				{
					logIt(cmpids);
					if (_tcsicmp(cmpids, _T("USB\\Class_02")) == 0)
					{
						ZeroMemory(b, sizeof(b));
						sz = sizeof(b);
						if ((ret=devstatus.procGetCommonHW(glabel, item, (PDEVPROPKEY)&DEVPKEY_Device_InstanceId, b, sz, type)) == ERROR_SUCCESS)
						{
							bFind = true;
							using namespace System;
							using namespace Microsoft::Win32;
							RegistryKey^ rk;
							String^ spath = String::Format("SYSTEM\\CurrentControlSet\\Enum\\{0}\\Device Parameters", gcnew String((TCHAR*)b));//HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Enum\USB\VID_04E8&PID_6860&Modem\6&dc9ff83&0&0001\Device Parameters
							rk = Registry::LocalMachine->OpenSubKey(spath, false);
							String^ sPort = (String^)(rk->GetValue("PortName"));
							if (!String::IsNullOrEmpty(sPort))
							{
								pin_ptr<const TCHAR> content = PtrToStringChars(sPort);
								TCHAR pBuf[100] = { 0 };
								_stprintf_s(pBuf, _T("%s"), content);
								logIt(_T("port found %s.\n"), pBuf);
								m_info["PortName"] = pBuf;
								bFind = TRUE;
								break;
							}

						}
						else
						{
							OOPSERR(ret);
						}
						break;
					}
					dwoffset += _tcslen(cmpids) + 1;
					cmpids = (TCHAR *)b + dwoffset;
				}

			}
			else
			{
				OOPSERR(ret);
			}
			if (bFind)break;
		}
	}
	else
	{
		ret = ERROR_NOT_FOUND;
		OOPSERR(ret);
	}

	EXIT_FUNCTRET(ret);
	return ret;
}

//BOOL EnumeratePortsNext(HANDLE DeviceInfoSet, LPTSTR lpBuffer)
//{
//	static CM_Open_DevNode_Key OpenDevNodeKey = NULL;
//	static HINSTANCE CfgMan;
//
//	int res1;
//	TCHAR DevName[MAX_NAME_PORTS] = { 0 };
//	static int numDev = 0;
//	int numport;
//
//	SP_DEVINFO_DATA DeviceInfoData = { 0 };
//	DeviceInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
//
//
//	if (!DeviceInfoSet || !lpBuffer)return -1;
//	if (!OpenDevNodeKey)
//	{
//		CfgMan = LoadLibrary(_T("cfgmgr32.dll"));
//		if (!CfgMan)return FALSE;
//		OpenDevNodeKey =
//			(CM_Open_DevNode_Key)GetProcAddress(CfgMan, "CM_Open_DevNode_Key");
//		if (!OpenDevNodeKey)
//		{
//			FreeLibrary(CfgMan);
//			return FALSE;
//		}
//	}
//
//	while (TRUE)
//	{
//
//		HKEY KeyDevice;
//		DWORD len;
//		res1 = SetupDiEnumDeviceInfo(
//			DeviceInfoSet, numDev, &DeviceInfoData);
//
//		if (!res1)
//		{
//			SetupDiDestroyDeviceInfoList(DeviceInfoSet);
//			FreeLibrary(CfgMan);
//			OpenDevNodeKey = NULL;
//			return FALSE;
//		}
//
//
//		res1 = OpenDevNodeKey(DeviceInfoData.DevInst, KEY_QUERY_VALUE, 0,
//			RegDisposition_OpenExisting, &KeyDevice, CM_REGISTRY_HARDWARE);
//		if (res1 != ERROR_SUCCESS)return NULL;
//		len = MAX_NAME_PORTS;
//
//		res1 = RegQueryValueEx(
//			KeyDevice,    // handle of key to query
//			_T("portname"),    // address of name of value to query
//			NULL,    // reserved
//			NULL,    // address of buffer for value type
//			(LPBYTE)DevName,    // address of data buffer
//			&len     // address of data buffer size
//			);
//
//		RegCloseKey(KeyDevice);
//		if (res1 != ERROR_SUCCESS)return NULL;
//		numDev++;
//		if (memicmp(DevName, _T("com"), 3*sizeof(TCHAR)))continue;
//		numport = _tstoi(DevName + 3);
//		if (numport > 0 && numport <= 256)
//		{
//			_tcscpy(lpBuffer, DevName);
//			return TRUE;
//		}
//
//		FreeLibrary(CfgMan);
//		OpenDevNodeKey = NULL;
//		return FALSE;
//	}
//}

BOOL  EndEnumeratePorts(HDEVINFO DeviceInfoSet)
{
	ENTER_FUNCTION();
	if (SetupDiDestroyDeviceInfoList(DeviceInfoSet))return TRUE;
	else return FALSE;
}

VOID WINAPI CompletionRoutine(_In_ DWORD dwErrorCode, _In_ DWORD dwNumberOfBytesTransfered, _Inout_ LPOVERLAPPED lpOverlapped)
{
	UNREFERENCED_PARAMETER(dwErrorCode);
	UNREFERENCED_PARAMETER(dwNumberOfBytesTransfered);
	UNREFERENCED_PARAMETER(lpOverlapped);
	//SetEvent(lpOverlapped->hEvent);

}

int FinalSize(double d) {
	int i = 1;
	for (int a = 0; a < 10; a++) {
		i <<= 1;
		if (d < i) {
			return i;
		}
	}
	return 0;
}

std::string FormatSize(long long bytes) {
	const long long GB = 1024 * 1024;
	const long long TB = GB * 1024;

	double size = bytes;

	if (bytes >= TB) {
		size = bytes / (double)TB;

		return to_string(FinalSize(size)) + " TB";
	}
	else if (bytes >= GB) {
		size = bytes / (double)GB;
		return to_string(FinalSize(size)) + " GB";
	}
	else {
		return to_string(bytes) + " MB";
	}
}

CString ParserSamsungSize(std::string fileContent) {
	// Remove 'AT+SIZECHECK='
	regex pattern("\\bAT\\+SIZECHECK=", regex_constants::icase);
	string step1 = regex_replace(fileContent, pattern, "");

	// Remove 'OK'
	pattern = regex("\\bOK\\b", regex_constants::icase);
	string step2 = regex_replace(step1, pattern, "");

	// Remove '\r' and '\\'
	string step3 = regex_replace(step2, regex("[\\r\\\\]"), "");

	// Replace ';' with new line
	string step4 = regex_replace(step3, regex(";"), "\n");

	// Find size of dev, system, data, and storage
	long long size = 0;
	//vector<string> lines;
	pattern = regex("^[^:]*:([0-9]+).*?(dev|system|data|storage)$", regex_constants::icase);
	for (sregex_iterator it(step4.begin(), step4.end(), pattern), end_it; it != end_it; ++it) {
		smatch match = *it;
		size += stoll(match[1].str());
	}
	if (size == 0) {
		return _T("");
	}
	else {
		CString sMode;
		sMode.Format(_T("%S"), FormatSize(size).c_str());
		return sMode;
	}	
}


std::string remove_special_chars(const std::string& str)
{
	std::string result = str;
	result.erase(std::remove_if(result.begin(), result.end(),
		[](unsigned char c) { return c == 0x2 || c == 0x3; }),
		result.end());
	return result;
}

CString ParserLGSize(std::string fileContent) {
	regex pattern("\\bAT%EMMCSIZE", regex_constants::icase);
	string step1 = regex_replace(fileContent, pattern, "");
	// Remove 'OK'
	pattern = regex("\\bOK\\b", regex_constants::icase);
	string step2 = regex_replace(step1, pattern, "");

	// Remove '\r' and '\n'
	string s = regex_replace(step2, regex("[\\r\\n]"), "");

	//std::replace(s.begin(), s.end(), '\x02', '\x20');
	//std::replace(s.begin(), s.end(), '\x03', '\x20');
	s = remove_special_chars(s);

	// Find size of dev, system, data, and storage
	long long size = 0;
	size = stoll(s.c_str())*1024;

	CString sMode;
	sMode.Format(_T("%S"), FormatSize(size).c_str());
	return sMode;
}

BOOL IsValidIMEI(CString sIMEI) {
	BOOL ret = FALSE;
	if (sIMEI.GetLength() < 14 || sIMEI.GetLength() > 17) {
		return ret;
	}
	
	for (int i = 0; i < sIMEI.GetLength(); i++) {
		if (sIMEI.GetAt(i) < L'0' || sIMEI.GetAt(i) > L'9') {
			return ret;
		}
	}
	//std::wregex wrx(L"\d{14,17}");
	//std::wstring imei = sIMEI;
	//std::wsmatch wideMatch;
	//if (std::regex_match(imei.cbegin(), imei.cend(), wideMatch, wrx)) {
	//	//_tprintf(_T("IMEI=%s\n"),  sIMEI);
	//	ret = TRUE;
	//}
	return TRUE;
}

void HandleRecvData_Active(char* data, char* skipstr, TCHAR *key, std::map<CString, CString> &info)
{
	ENTER_FUNCTION();
	logIt(_T("Recv:%S"), data);
	if (strlen(data) == 0)
	{
		m_info[key] = _T("NORETURN");
		_tprintf(_T("%s=%s\n"), key, _T("NORETURN"));
	}
	else if (strstr(data, "\r\nERROR\r\n"))
	{
		m_info[key] = _T("NOTSUPPORT");
		_tprintf(_T("%s=%s\n"), key, _T("NOTSUPPORT"));
	}
	else
	{
		if (strstr(data, "\r\nOK") != NULL)
		{
			if (strstr(data, "1,UNLOCK")||strstr(data,"1,NG("))
			{
				m_info[key] = _T("UNLOCK");
				//_tprintf(_T("%s=%s\n"), key, _T("UNLOCK"));
			}
			else if (strstr(data, "1,LOCK") || strstr(data, "1,TRIGGERED"))
			{
				m_info[key] = _T("LOCK");
				//_tprintf(_T("%s=%s\n"), key, _T("LOCK"));
			}
			else
			{
				logIt(_T("can not parser:%s\n"), data);
				m_info[key] = _T("NOSUPPORT");
			}
			_tprintf(_T("%s=%s\n"), key, m_info[key]);
		}
		else
		{
			m_info[key] = _T("NOSUPPORT");
			_tprintf(_T("%s=%s\n"), key, _T("NOSUPPORT"));
		}

	}

}

void HandleRecvData(char* data, char *atcmd, char* skipstr, TCHAR *key, std::map<CString, CString> &info)
{
	ENTER_FUNCTION();
	logIt(_T("Recv:%S"), data);
	if (strlen(data) == 0) return;
	if (strstr(data, "\r\nERROR\r\n")|| strstr(data, "+CME Error") || strstr(data, "+CME ERROR"))
	{
		m_info[key] = _T("");
		//_tprintf(_T("%s=%s\n"), key, _T("NOTSUPPORT"));
	}
	else
	{
		char* p = strstr(data, atcmd);
		if (NULL != p)
		{
			p += strlen(atcmd);
		}
		else
		{
			p = data;
		}
		char *pp = strstr(p, skipstr);
		if (pp != NULL)
		{
			pp += strlen(skipstr);
		}
		else
		{
			pp = p;
		}
		char *ppp = strstr(pp, "\r\nOK\r\n");
		if (ppp != NULL) ppp[0] = 0;
		CString sMode; 
		sMode.Format(_T("%S"), pp);
		sMode.Replace(_T("\r"), _T(""));
		sMode.Replace(_T("\n"), _T(""));
		sMode.Trim();
		if (sMode.GetLength() > 0)
		{
			m_info[key] = sMode;
			if (_tcsncicmp(key, _T("IMEI"), 4) != 0) {
				_tprintf(_T("%s=%s\n"), key, sMode);
			}
			else {
				if (IsValidIMEI(m_info[_T("IMEI")])) {
					_tprintf(_T("IMEI=%s\n"), m_info[_T("IMEI")]);
				}
				else {
					m_info[key] = _T("");
				}
			}
		}
	}
}
void HandleRecvDataLG(char* data, char *atcmd, char* skipstr, TCHAR *key, std::map<CString, CString> &info)
{
	ENTER_FUNCTION();
	logIt(_T("Recv:%S"), data);
	if (strlen(data) == 0) return;
	if (strstr(data, "\r\nERROR\r\n") || strstr(data, "+CME Error"))
	{
		m_info[key] = _T("");
		//_tprintf(_T("%s=%s\n"), key, _T("NOTSUPPORT"));
	}
	else
	{
		char* p = strstr(data, atcmd);
		if (NULL != p)
		{
			p += strlen(atcmd);
		}
		else
		{
			p = data;
		}
		char *pp = strstr(p, skipstr);
		if (pp != NULL)
		{
			pp += strlen(skipstr);
		}
		else
		{
			pp = p;
		}
		char *ppp = strstr(pp, "\r\nOK\r\n");
		if (ppp != NULL) ppp[0] = 0;
		CString sMode;
		sMode.Format(_T("%S"), pp);
		sMode.Replace(_T("\r"), _T(""));
		sMode.Replace(_T("\n"), _T(""));
		sMode.Replace(_T("\x02"), _T(""));
		sMode.Replace(_T("\x03"), _T(""));
		sMode.Trim();
		if (sMode.GetLength() > 0)
		{
			m_info[key] = sMode;
			if (_tcsncicmp(key, _T("IMEI"), 4) != 0) {
				_tprintf(_T("%s=%s\n"), key, sMode);
			}
			else {
				if (IsValidIMEI(m_info[_T("IMEI")])) {
					_tprintf(_T("IMEI=%s\n"), m_info[_T("IMEI")]);
				}
				else {
					m_info[key] = _T("");
				}
			}
		}
	}
}
DWORD GetDeviceInfoLG(TCHAR *HubName, int nPort, BOOL bGetInfo, BOOL bGetActiveLock) {
	ENTER_FUNCTION();
	DWORD ret = ERROR_INVALID_PARAMETER;
	TCHAR lcation[1024] = { 0 };
	GetHubPortLocationpath(HubName, nPort, lcation, 1024);
	if (_tcslen(lcation) > 0)
	{
		m_info[_T("HubPortLocationPath")] = lcation;
	}
	GetModemFromHub(HubName, nPort);
	HDEVINFO h = BeginEnumeratePorts();
	if (h != NULL)
	{
		TCHAR aa[1024] = { 0 };
		if (m_info.find(_T("PortName")) == m_info.end())
		{
			EnumeratePortsNext(h, aa, 1024, lcation);
			if (_tcslen(aa) > 0)
			{
				m_info[_T("PortName")] = aa;
			}
		}
		EndEnumeratePorts(h);

		if (m_info.find(_T("PortName")) == m_info.end() || _tcslen(m_info[_T("PortName")]) == 0)
		{
			ret = ERROR_NOT_FOUND;
			return ret;
		}
		if (_tcslen(m_info[_T("PortName")]) > 0 && !bGetInfo)
		{
			return ERROR_SUCCESS;
		}
		CSerialPort sp;
		map<string, string> atrespone;
		//HANDLE hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
		extern int gTimeOut;

		try {
			ret = sp.OpenComm((LPCTSTR)m_info[_T("PortName")], (DWORD)CBR_115200);
			if (ERROR_SUCCESS != ret) {
				Sleep(3000);
				ret = sp.OpenComm((LPCTSTR)m_info[_T("PortName")], (DWORD)CBR_115200);
			}
			if (ERROR_SUCCESS != ret) {
				return ret;
			}
			char retdata[1024] = { 0 }; DWORD dwLen = 1024;
			BYTE *response = (BYTE *)retdata;
			DWORD _start = GetTickCount();
			if (ERROR_SUCCESS == ret) {
				do {
					ZeroMemory(retdata, 1024); dwLen = 1024;
					sp.Send_Recv((BYTE *)"ATZ\r", strlen("ATZ\r"), response, dwLen, 4000);
					sp.Send_Recv((BYTE *)"AT\r", strlen("AT\r"), response, dwLen, 1000);
					//AT%BATTLEVEL
					ZeroMemory(retdata, 1024); dwLen = 1204;
					sp.Send_Recv((BYTE *)"AT%BATTLEVEL\r", strlen("AT%BATTLEVEL\r"), response, dwLen);
					atrespone["BATTERY"] = hexStr(response, dwLen);
					if (strlen(retdata) > 0 && strstr(retdata + dwLen - 6, "\r\nOK\r\n") != NULL)
					{
						HandleRecvDataLG(retdata, "AT%BATTLEVEL\r", "", _T("Battery"), m_info);
					}
					//AT%EMMCSIZE     //MB
					ZeroMemory(retdata, 1024); dwLen = 1204;
					sp.Send_Recv((BYTE *)"AT%EMMCSIZE\r", strlen("AT%EMMCSIZE\r"), response, dwLen);
					atrespone["EMMCSIZE"] = hexStr(response, dwLen);
					if (strlen(retdata) > 0 && strstr(retdata + dwLen - 6, "\r\nOK\r\n") != NULL)
					{
						CString sSize = ParserLGSize(retdata);
						m_info[_T("Size")] = sSize;
						_tprintf(_T("Size=%s\n"), sSize);
					}
					//AT%OSVER
					ZeroMemory(retdata, 1024); dwLen = 1204;
					sp.Send_Recv((BYTE *)"AT%OSVER\r", strlen("AT%OSVER\r"), response, dwLen);
					atrespone["OSVER"] = hexStr(response, dwLen);
					if (strlen(retdata) > 0 && strstr(retdata + dwLen - 6, "\r\nOK\r\n") != NULL)
					{
						HandleRecvDataLG(retdata, "AT%OSVER\r", "", _T("Version"), m_info);
					}

					//AT%OSVER
					ZeroMemory(retdata, 1024); dwLen = 1204;
					sp.Send_Recv((BYTE *)"AT%IMEI\r", strlen("AT%IMEI\r"), response, dwLen);
					atrespone["ATIMEI"] = hexStr(response, dwLen);
					if (strlen(retdata) > 0 && strstr(retdata + dwLen - 6, "\r\nOK\r\n") != NULL)
					{
						HandleRecvDataLG(retdata, "AT%IMEI\r", "", _T("IMEI"), m_info);
					}
					//AT%KSUNLOCKSTATUS
					if (m_info.find(_T("IMEI")) == m_info.end() || (m_info[_T("IMEI")].GetLength() == 0)) 
					{
						///////////////IMEI READ BEGIN////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						for (int i = 0; i < 3; i++)
						{
							ZeroMemory(retdata, 1024); dwLen = 1204;
							sp.Send_Recv((BYTE *)"ATI\r", strlen("ATI\r"), response, dwLen);
							atrespone["ATI"] = hexStr(response, dwLen);
							if (strlen(retdata) > 0 && strstr(retdata + dwLen - 6, "\r\nOK\r\n") != NULL)
							{
								ATIINFOResponse(retdata, m_info);
							}
							if (m_info.find(_T("IMEI")) != m_info.end() &&
								(m_info[_T("IMEI")].Find(_T("000000000000000")) == 0) || (m_info[_T("IMEI")].GetLength() == 0))
							{
								Sleep(5000);
								//May be Not ready. return 0000000000000000000
							}
							else
								break;
						}
					}

					DWORD d = GetTickCount();
					if (d - _start > gTimeOut * 1000) {
						break;
					}
					if (m_info.find(_T("IMEI")) != m_info.end()) {
						if (!IsValidIMEI(m_info[_T("IMEI")])) {
							m_info[_T("IMEI")] = _T("");
						}
					}
				} while (m_info.find(_T("IMEI")) == m_info.end() || m_info[_T("IMEI")].GetLength() == 0);

			}
			else {
				logIt(_T("Open serial Port Failed."));
			}

		}
		catch (...) {
			OOPS();
		}
		finally
		{
			QueryBlackList();
			sp.Close();
		}
	}
	else
	{
		ret = GetLastError();
		OOPSERR(ret);
	}
	return ret;

}

/*
ATI1
Manufacturer: SAMSUNG ELECTRONICS CORPORATION
Model: SPH-L710_VMU
Revision: L710VPAMG2
IMEI: 990003342339740
+GCAP: +CGSM

OK

AT+DEVCONINFO
+DEVCONINFO: MN(SPH-L710);BASE(GT-I9300);VER(L710VPAMG2/L710SPTAMG2/L710VPAMG2/L710VPAMG2);HIDVER(L710VPAMG2/L710SPTAMG2/L710VPAMG2/L710VPAMG2);MNC(000);MCC(310);PRD(XXXXXXXXXXXXAS);SN(00000000000);IMEI(99000334233974);PN(0000005956);CON(AT,MTP);LOCK(NONE);LIMIT(FALSE);SDP(RUNTIME);HVID(Data:196609,Cache:262145,System:327681)
#OK#

OK
AT+IMEINUM

+IMEINUM: 99000334233974AT+SWVER

AT+SWVER
+SWVER: L710VPAMG2/L710VPAMG2/L710SPTAMG2
OK


*/
DWORD GetDeviceInfo(TCHAR *HubName, int nPort, BOOL bGetInfo, BOOL bGetActiveLock)
{
	ENTER_FUNCTION();
	DWORD ret = ERROR_INVALID_PARAMETER;
	TCHAR lcation[1024] = { 0 };
	GetHubPortLocationpath(HubName, nPort, lcation, 1024);
	if (_tcslen(lcation) > 0)
	{
		m_info[_T("HubPortLocationPath")] = lcation;
	}
	GetModemFromHub(HubName, nPort);
	HDEVINFO h = BeginEnumeratePorts();
	if (h != NULL)
	{
		TCHAR aa[1024] = { 0 };
		if (m_info.find(_T("PortName")) == m_info.end())
		{
			EnumeratePortsNext(h, aa, 1024, lcation);
			if (_tcslen(aa) > 0)
			{
				m_info[_T("PortName")] = aa;
			}
		}
		EndEnumeratePorts(h);

		if (m_info.find(_T("PortName"))==m_info.end()||_tcslen(m_info[_T("PortName")]) == 0)
		{
			ret = ERROR_NOT_FOUND;
			return ret;
		}
		if (_tcslen(m_info[_T("PortName")]) > 0 && !bGetInfo)
		{
			return ERROR_SUCCESS;
		}
		CSerialPort sp;
		map<string, string> atrespone;
		//HANDLE hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
		extern int gTimeOut;
		
		try{
			ret = sp.OpenComm((LPCTSTR)m_info[_T("PortName")], (DWORD)CBR_115200);
			if (ERROR_SUCCESS != ret) {
				Sleep(3000);
				ret = sp.OpenComm((LPCTSTR)m_info[_T("PortName")], (DWORD)CBR_115200);
			}
			if (ERROR_SUCCESS != ret) {
				return ret;
			}
			char retdata[4096] = { 0 }; DWORD dwLen = 1024;
			BYTE *response = (BYTE *)retdata;
			DWORD _start = GetTickCount();
			if (ERROR_SUCCESS == ret) {
				do {
					ZeroMemory(retdata, 1024); dwLen = 1024;
					sp.Send_Recv((BYTE *)"ATV1\r", strlen("ATV1\r"), response, dwLen, 1000);
					dwLen = 1024;
					sp.Send_Recv((BYTE *)"AT+CMEE=0\r", strlen("AT+CMEE=0\r"), response, dwLen, 1000);


					///////////////IMEI READ BEGIN////////////////////////////////////////////////////////////////////////////////////////////////////////////////
					for (int i = 0; i < 3; i++)
					{
						ZeroMemory(retdata, 1024); dwLen = 1204;
						sp.Send_Recv((BYTE *)"AT+IMEINUM\r", strlen("AT+IMEINUM\r"), response, dwLen);
						atrespone["AT+IMEINUM"] = hexStr(response, dwLen);
						if (strlen(retdata) > 0 && strstr(retdata + dwLen - 6, "\r\nOK\r\n") != NULL)
						{
							HandleRecvData(retdata, "AT+IMEINUM\r", "+IMEINUM:", _T("IMEI"), m_info);
						}
						if (m_info.find(_T("IMEI")) != m_info.end() && m_info[_T("IMEI")].Find(_T("000000000000000")) == 0)
						{
							Sleep(5000);
							//May be Not ready. return 0000000000000000000
						}
						else
							break;
					}

					if (m_info.find(_T("IMEI")) == m_info.end() || m_info[_T("IMEI")].GetLength() == 0)
					{
						ZeroMemory(retdata, 1024); dwLen = 1204;
						sp.Send_Recv((BYTE *)"AT+GSN\r", strlen("AT+GSN\r"), response, dwLen);  //SM-G920P Only support AT+GSN
						atrespone["AT+GSN"] = hexStr(response, dwLen);
						if (strlen(retdata) > 0 && strstr(retdata + dwLen - 6, "\r\nOK\r\n") != NULL)
						{
							HandleRecvData(retdata, "AT+GSN\r", "+GSN:", _T("IMEI"), m_info);
						}
						if (m_info.find(_T("IMEI")) != m_info.end()) {
							if (!IsValidIMEI(m_info[_T("IMEI")])) {
								dwLen = 1204;
								sp.Send_Recv((BYTE *)"AT+CGSN\r", strlen("AT+CGSN\r"), response, dwLen);  //SM-G920P Only support AT+GSN
								atrespone["AT+CGSN"] = hexStr(response, dwLen);
								if (strlen(retdata) > 0)
								{
									HandleRecvData(retdata, "AT+CGSN\r", "+CGSN:", _T("IMEI"), m_info);
								}
							}
						}
						
					}
					///////////////IMEI READ END//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

					ZeroMemory(retdata, 1024); dwLen = 1024;
					sp.Send_Recv((BYTE *)"AT+DEVCONINFO\r\n", strlen("AT+DEVCONINFO\r\n"), response, dwLen);//
					atrespone["AT+DEVCONINFO"] = hexStr(response, dwLen);
					DevconINFOResponse(retdata, m_info);
					HandleRecvData(retdata, "AT+DEVCONINFO\r", "+DEVCONINFO:", _T("DEVCONINFO"), m_info);

					BOOL bModel = false;
					if (m_info.find(_T("MN")) != m_info.end()) {
						m_info[_T("Model")] = m_info[_T("MN")];
						_tprintf(_T("Model=%s\n"), m_info[_T("MN")]);
						bModel = true;
					}
					if (m_info.find(_T("IMEI")) != m_info.end()) {
						if (!IsValidIMEI(m_info[_T("IMEI")])) {
							m_info[_T("IMEI")] = m_info.find(_T("IMEID")) != m_info.end() ? m_info[_T("IMEID")] : _T("");
							_tprintf(_T("IMEI=%s\n"), m_info[_T("IMEI")]);
						}
					}

					ZeroMemory(retdata, 4096); dwLen = 4096;
					sp.Send_Recv((BYTE *)"AT+SIZECHECK=\r", strlen("AT+SIZECHECK=\r"), response, dwLen);
					if (strlen(retdata) > 0 && strstr(retdata + dwLen - 6, "\r\nOK\r\n") != NULL) {
						CString sSize = ParserSamsungSize(retdata);
						m_info[_T("Size")] = sSize;
						_tprintf(_T("Size=%s\n"), sSize);
					}

					if (bGetActiveLock) { // now not to do it. not update data to CMC
						ZeroMemory(retdata, 1024); dwLen = 1024;
						sp.Send_Recv((BYTE *)"AT+GMI\r", strlen("AT+GMI\r"), response, dwLen, 3000);//SM-G920P Only support AT+GMM 
						atrespone["AT+GMI"] = hexStr(response, dwLen);
						if (strlen(retdata) > 0 && strstr(retdata + dwLen - 6, "\r\nOK\r\n") != NULL)
						{
							HandleRecvData(retdata, "AT+GMI\r", "GMI:", _T("Maker"), m_info);
						}
						else
						{
							ZeroMemory(retdata, 1024); dwLen = 1024;
							sp.Send_Recv((BYTE *)"AT+CGMI\r", strlen("AT+CGMI\r"), response, dwLen, 3000);//SM-G920P Only support AT+GMM
							atrespone["AT+CGMI"] = hexStr(response, dwLen);
							if (strlen(retdata) > 0)
							{
								HandleRecvData(retdata, "AT+CGMI\r", "+CGMI:", _T("Maker"), m_info);
							}
						}

						
						//Samsung T399 Send AT+GMM SGH-T399, Send AT+CGMM SAMSUNG
						if (!bModel) {
							ZeroMemory(retdata, 1024); dwLen = 1024;
							sp.Send_Recv((BYTE *)"AT+GMM\r", strlen("AT+GMM\r"), response, dwLen,3000);//SM-G920P Only support AT+GMM 
							atrespone["AT+GMM"] = hexStr(response, dwLen);
							if (strlen(retdata) > 0 && strncmp(retdata, "Samsung", 7) != 0 && strstr(retdata + dwLen - 6, "\r\nOK\r\n") != NULL)
							{
								HandleRecvData(retdata, "AT+GMM\r", "GMM:", _T("Model"), m_info);
							}
							else
							{
								ZeroMemory(retdata, 1024); dwLen = 1024;
								sp.Send_Recv((BYTE *)"AT+CGMM\r", strlen("AT+CGMM\r"), response, dwLen, 3000);//SM-G920P Only support AT+GMM
								atrespone["AT+CGMM"] = hexStr(response, dwLen);
								if (strlen(retdata) > 0)
								{
									HandleRecvData(retdata, "AT+CGMM\r", "+CGMM:", _T("Model"), m_info);
								}
							}
						}
						ZeroMemory(retdata, 1024); dwLen = 1024;
						sp.Send_Recv((BYTE *)"AT+GMR\r", strlen("AT+GMR\r"), response, dwLen, 3000);//SM-G920P Only support AT+GMM 
						atrespone["AT+GMR"] = hexStr(response, dwLen);
						if (strlen(retdata) > 0 && strstr(retdata + dwLen - 6, "\r\nOK\r\n") != NULL)
						{
							HandleRecvData(retdata, "AT+GMR\r", "GMR:", _T("Version"), m_info);
						}
						else
						{
							ZeroMemory(retdata, 1024); dwLen = 1024;
							sp.Send_Recv((BYTE *)"AT+CGMR\r", strlen("AT+CGMR\r"), response, dwLen, 3000);//SM-G920P Only support AT+GMM
							atrespone["AT+CGMR"] = hexStr(response, dwLen);
							if (strlen(retdata) > 0)
							{
								HandleRecvData(retdata, "AT+CGMR\r", "+CGMR:", _T("Version"), m_info);
							}
						}
					}
					DWORD d = GetTickCount();
					if (d - _start > gTimeOut * 1000) {
						break;
					}
					if (m_info.find(_T("IMEI")) != m_info.end()) {
						if (!IsValidIMEI(m_info[_T("IMEI")])) {
							m_info[_T("IMEI")] = _T("");
						}
					}
				} while (m_info.find(_T("IMEI")) == m_info.end() || m_info[_T("IMEI")].GetLength() == 0);

			}else {
				logIt(_T("Open serial Port Failed."));
			}
			CString spModel = m_info.find(_T("Model")) != m_info.end() ? m_info[_T("Model")] : _T("");
			CString spVer = m_info.find(_T("Version")) != m_info.end() ? m_info[_T("Version")] : _T("");
			if (spVer.GetLength() == 0) {
				CString str = m_info.find(_T("VER")) != m_info.end() ? m_info[_T("VER")] : _T("");
				int nTokenPos = 0;
				spVer = str.Tokenize(_T("/"), nTokenPos);				
			}
			////for Centex 
			extern BOOL gReadLock;
			if (gReadLock) {
				if (bGetActiveLock &&  QueryRecordFrp(spModel, spVer) != 0)
				{
					sp.Purge(0xF);
					for (int i = 0; i < 2; i++) {
						ZeroMemory(retdata, 1024); dwLen = 1024;
						sp.Send_Recv((BYTE *)"AT+REACTIVE=1,0,0\r\n", strlen("AT+REACTIVE=1,0,0\r\n"), response, dwLen);//
						atrespone["AT+REACTIVE"] = hexStr(response, dwLen);
						m_info[_T("ORIG_REACTIVE")] = s2ws(hexStr(response, dwLen)).c_str();
						_tprintf(_T("%s=%s\n"), _T("ORIG_REACTIVE"), s2ws(hexStr(response, dwLen)).c_str());
						HandleRecvData_Active(retdata, "AT+REACTIVE=1,0,0\r", _T("REACTIVE"), m_info);
						if (m_info[_T("REACTIVE")] == _T("LOCK") || m_info[_T("REACTIVE")] == _T("UNLOCK")) {
							break;
						}
					}
				}
				else
				{
					_tprintf(_T("%s=%s\n"), _T("REACTIVE"), _T("UNLOCK"));
				}
			}
			
		}
		catch(...){
			OOPS();
		}
		finally
		{
			QueryBlackList();
			sp.Close();
		}
	}
	else
	{
		ret = GetLastError();
		OOPSERR(ret);
	}
	return ret;
}

void QueryBlackList() {
	CString spModel = m_info.find(_T("Model")) != m_info.end() ? m_info[_T("Model")] : _T("");
	CString sVER = m_info.find(_T("VER")) != m_info.end() ? m_info[_T("VER")] : _T("");
	if (spModel.GetLength() == 0 || sVER.GetLength() == 0) {
		_tprintf(_T("%s=%s\n"), _T("BLACKLIST"), _T("unknown"));
		return;
	}
	
	int nTokenPos = 0;
	CString strToken = sVER.Tokenize(_T("/"), nTokenPos);
	std::set<CString> vers;
	while (!strToken.IsEmpty())
	{
		vers.insert(strToken);
		// ....
		strToken = sVER.Tokenize(_T("/"), nTokenPos);
	}

	CString bl = LookForBlackList(vers, spModel);
	if (bl.GetLength() == 0) {
		_tprintf(_T("%s=%s\n"), _T("BLACKLIST"), _T("missing"));
	}
	else {
		_tprintf(_T("%s=%s\n"), _T("BLACKLIST"), bl);
	}

}

DWORD DoRefurbishLG(TCHAR *sHubName, int nPort)
{
	ENTER_FUNCTION();
	DWORD dwret = ERROR_INVALID_PARAMETER;
	if (m_info.find(_T("PortName")) == m_info.end())
	{
		dwret = GetDeviceInfo(sHubName, nPort, FALSE);
		if (dwret != ERROR_SUCCESS)
		{
			OOPSERR(dwret);
		}
	}

	CSerialPort sp;
	BOOL bOKReturn = false;
	int iretry = 0;
	do {
		try {

			m_info[_T("FRST")] = _T("Failed");

			dwret = sp.OpenComm((LPCTSTR)m_info[_T("PortName")], (DWORD)CBR_115200);

			char ret[1024] = { 0 }; DWORD dwLen = 1024;
			BYTE *response = (BYTE *)ret;
			sp.Send_Recv((BYTE*)"AT\r", strlen("AT\r"), response, dwLen, 1000);
			

			ZeroMemory(ret, 1024); dwLen = 1024;
			sp.Send_Recv((BYTE*)"AT%FRST\r", strlen("AT%FRST\r"), response, dwLen, 30000, 1, 1000);
			if (strlen(ret) > 0)
			{
				logIt(_T("Recv:%S"), ret);
				if (strstr(ret, "OK") != NULL)
				{
					m_info[_T("FACTORST")] = _T("Success");
					_tprintf(_T("%s=%s\n"), _T("FACTORST"), _T("Success"));
					bOKReturn = true;
					dwret = ERROR_SUCCESS;
				}
				else
				{
					m_info[_T("FACTORST")] = _T("Error");
					_tprintf(_T("%s=%s\n"), _T("FACTORST"), _T("Error"));
					dwret = ERROR_NOT_SUPPORTED;
				}
			}
			else
			{
				dwret = ERROR_NOT_SUPPORTED;
				OOPSERR(dwret);
			}
		}
		catch (...) {
			dwret = ERROR_INVALID_EXCEPTION_HANDLER;
			OOPSERR(dwret);
		}
		finally
		{
			sp.Close();
		}

	} while (iretry++ < 3 && !bOKReturn);
	//wait for rebooting
	if (dwret == ERROR_SUCCESS && gWaitDeviceGone)
	{
		dwret = CheckRefurbishReboot(sHubName, nPort, gTimeOut * 1000);
	}

	return dwret;
}


DWORD DoRefurbish(TCHAR *sHubName, int nPort)
{
	ENTER_FUNCTION();
	DWORD dwret = ERROR_INVALID_PARAMETER;
	if (m_info.find(_T("PortName")) == m_info.end())
	{
		dwret = GetDeviceInfo(sHubName, nPort, FALSE);
		if (dwret != ERROR_SUCCESS)
		{
			OOPSERR(dwret);
		}
	}
	
	CSerialPort sp;
	BOOL bOKReturn = false;
	int iretry = 0;
	do{
		try{

			m_info[_T("FACTORST")] = _T("Failed");

			dwret = sp.OpenComm((LPCTSTR)m_info[_T("PortName")], (DWORD)CBR_115200);
			if (ERROR_SUCCESS != dwret) {
				Sleep(3000);
				dwret = sp.OpenComm((LPCTSTR)m_info[_T("PortName")], (DWORD)CBR_115200);
			}
			if (ERROR_SUCCESS != dwret) {
				return dwret;
			}
			char ret[1024] = { 0 }; DWORD dwLen = 1024;
			BYTE *response = (BYTE *)ret;
			sp.Send_Recv((BYTE*)"AT\r", strlen("AT\r"), response, dwLen, 1000);
			/*dwLen = 1024;
			sp.Send_Recv((BYTE *)"ATV1\r", strlen("ATV1\r"), response, dwLen, 1000);*/
			dwLen = 1024;
			sp.Send_Recv((BYTE *)"AT+CMEE=0\r", strlen("AT+CMEE=0\r"), response, dwLen, 1000);

			ZeroMemory(ret, 1024); dwLen = 1024;
			sp.Send_Recv((BYTE*)"AT+FACTORST=0,0\r", strlen("AT+FACTORST=0,0\r"), response, dwLen, 30000);
			if (strlen(ret) > 0)
			{
				logIt(_T("Recv:%S"), ret);
				if (strstr(ret, "\r\nOK") != NULL)
				{
					m_info[_T("FACTORST")] = _T("Success");
					_tprintf(_T("%s=%s\n"), _T("FACTORST"), _T("Success"));
					bOKReturn = true;
					dwret = ERROR_SUCCESS;
				}
				else
				{
					m_info[_T("FACTORST")] = _T("Error");
					_tprintf(_T("%s=%s\n"), _T("FACTORST"), _T("Error"));
					dwret = ERROR_NOT_SUPPORTED;
				}
			}
			else
			{
				dwret = ERROR_NOT_SUPPORTED;
				OOPSERR(dwret);
			}
		}
		catch (...){
			dwret = ERROR_INVALID_EXCEPTION_HANDLER;
			OOPSERR(dwret);
		}
		finally
		{
			sp.Close();
		}

	} while (iretry++ < 3 && !bOKReturn);
	//wait for rebooting
	if (dwret == ERROR_SUCCESS && gWaitDeviceGone)
	{
		dwret = CheckRefurbishReboot(sHubName, nPort, gTimeOut*1000);
	}

	return dwret;
}

DWORD DoReactivation(TCHAR *sHubName, int nPort)
{
	ENTER_FUNCTION();
	DWORD ret = ERROR_INVALID_PARAMETER;
	if (m_info.find(_T("PortName")) == m_info.end())
	{
		GetDeviceInfo(sHubName, nPort, FALSE);
	}

	CSerialPort sp;
	try{

		m_info[_T("REACTIVATION")] = _T("Failed");


		if (sp.OpenComm((LPCTSTR)m_info[_T("PortName")], (DWORD)CBR_115200) == ERROR_SUCCESS)
		{
			int iRetry = 0;
			do
			{
				char retdata[1024] = { 0 }; DWORD dwLen = 1024;
				BYTE *response = (BYTE *)retdata;
				DWORD dwRet = sp.Send_Recv((BYTE*)"AT+REACTIVE=1,0,0\r\n", strlen("AT+REACTIVE=1,0,0\r\n"), response, dwLen);
				logIt(_T("reactivation return %d\n"), dwRet);
				HandleRecvData_Active(retdata, "AT+REACTIVE=1,0,0\r", _T("REACTIVE"), m_info);
				if (m_info[_T("REACTIVE")].CompareNoCase(_T("LOCK")) == 0 || m_info[_T("REACTIVE")].CompareNoCase(_T("UNLOCK")) == 0)
					break;
				Sleep(2000);
			} while (iRetry++ < 3);

		}
	}
	catch (...){
		OOPS();
	}
	finally
	{
		sp.Close();
	}
	return ret;

}

BOOL IsNoConnected(TCHAR *sHubName, int nPort, CString& sDriverKey)
{
	ENTER_FUNCTION();

	TCHAR* ConnectionStatuses[] =
	{
		_T("No Connected"),       // 0  - NoDeviceConnected
		_T("Connected"),          // 1  - DeviceConnected
		_T("FailedEnumeration"),  // 2  - DeviceFailedEnumeration
		_T("GeneralFailure"),     // 3  - DeviceGeneralFailure
		_T("Overcurrent"),        // 4  - DeviceCausedOvercurrent
		_T("NotEnoughPower"),     // 5  - DeviceNotEnoughPower
		_T("NotEnoughBandwidth"), // 6  - DeviceNotEnoughBandwidth
		_T("HubNestedTooDeeply"), // 7  - DeviceHubNestedTooDeeply
		_T("InLegacyHub"),        // 8  - DeviceInLegacyHub
		_T("Enumerating"),        // 9  - DeviceEnumerating
		_T("Reset")               // 10 - DeviceReset
	};

	BOOL bRet = false;
	SECURITY_ATTRIBUTES SA = { 0 };
	SA.nLength = sizeof(SECURITY_ATTRIBUTES);
	HANDLE HubHandle = CreateFile(sHubName, GENERIC_READ, FILE_SHARE_WRITE|FILE_SHARE_READ, &SA, OPEN_EXISTING, 0, NULL);
	if (HubHandle != INVALID_HANDLE_VALUE)
	{
		DWORD BytesReturned = 0;
		DWORD data_len = sizeof(USB_NODE_CONNECTION_INFORMATION_EX) + sizeof(USB_PIPE_INFO) * 32;
		PUSB_NODE_CONNECTION_INFORMATION_EX conn_info = (PUSB_NODE_CONNECTION_INFORMATION_EX)(new UCHAR[data_len]);
		conn_info->ConnectionIndex = nPort;

		if (!DeviceIoControl(HubHandle, IOCTL_USB_GET_NODE_CONNECTION_INFORMATION_EX, conn_info, data_len, conn_info, data_len, &data_len, 0))
		{
			OOPS();
		}
		logIt(_T("device connect status: %s\n"), ConnectionStatuses[conn_info->ConnectionStatus]);
		if (conn_info->ConnectionStatus != NoDeviceConnected)
		{
			UCHAR driverkey[1024] = { 0 };
			data_len = sizeof(UCHAR) * 1024;
			PUSB_NODE_CONNECTION_DRIVERKEY_NAME driverKeyName = (PUSB_NODE_CONNECTION_DRIVERKEY_NAME)driverkey;
			driverKeyName->ConnectionIndex = nPort;
			if (DeviceIoControl(HubHandle,
				IOCTL_USB_GET_NODE_CONNECTION_DRIVERKEY_NAME,
				driverKeyName,
				data_len,
				driverKeyName,
				data_len,
				&data_len,
				NULL))
			{
				sDriverKey = driverKeyName->DriverKeyName;
			}
		}

		if (conn_info->ConnectionStatus == NoDeviceConnected)
		{
			OOPS();
			bRet = TRUE;
		}
		else if (conn_info->ConnectionStatus == DeviceNotEnoughPower)
		{
			OOPS();
		}
		delete conn_info;
		CloseHandle(HubHandle);
	}
	else
	{
		OOPS();
	}
	return bRet;
}

DWORD CheckConnectDevice(TCHAR *sHubName, int nPort, int nPid, int nVid)
{
	DWORD dwRet = ERROR_INVALID_PARAMETER;
	ENTER_FUNCTION();

	TCHAR symblName[1024] = { 0 };
	if (sHubName == NULL || _tcslen(sHubName) == 0)
		return ERROR_INVALID_PARAMETER;

	if ((_tcsncicmp(sHubName, _T("\\\\?\\"), 4) == 0) || (_tcsncicmp(sHubName, _T("\\\\.\\"), 4) == 0))
	{
		_stprintf_s(symblName, _T("%s"), sHubName);
	}
	else
	{
		_stprintf_s(symblName, _T("%s%s"), _T("\\\\?\\"), sHubName);
	}

	SECURITY_ATTRIBUTES SA = { 0 };
	SA.nLength = sizeof(SECURITY_ATTRIBUTES);
	HANDLE HubHandle = CreateFile(symblName, GENERIC_READ, FILE_SHARE_WRITE | FILE_SHARE_READ, &SA, OPEN_EXISTING, 0, NULL);
	if (HubHandle != INVALID_HANDLE_VALUE)
	{
		DWORD BytesReturned = 0;
		DWORD data_len = sizeof(USB_NODE_CONNECTION_INFORMATION_EX) + sizeof(USB_PIPE_INFO) * 32;
		PUSB_NODE_CONNECTION_INFORMATION_EX conn_info = (PUSB_NODE_CONNECTION_INFORMATION_EX)(new UCHAR[data_len]);
		if (conn_info != NULL)
		{
			conn_info->ConnectionIndex = nPort;

			if (!DeviceIoControl(HubHandle, IOCTL_USB_GET_NODE_CONNECTION_INFORMATION_EX, conn_info, data_len, conn_info, data_len, &data_len, 0))
			{
				dwRet = GetLastError();
				OOPS();
			}
			else
			{
				logIt(_T("connect status %d, VID=%04X PID=%04X\n"), conn_info->ConnectionStatus, conn_info->DeviceDescriptor.idVendor, conn_info->DeviceDescriptor.idProduct);
				if (conn_info->ConnectionStatus != NoDeviceConnected)
				{
					if (nVid == 0 && nPid == 0)
					{
						dwRet = ERROR_CONTINUE;
					}
					else
					{
						if ((nVid == 0 || conn_info->DeviceDescriptor.idVendor == nVid))
						{
							if (nPid != 0 && (conn_info->DeviceDescriptor.idProduct != nPid))
								dwRet = ERROR_DEVICE_FEATURE_NOT_SUPPORTED;
							else
								dwRet = ERROR_SUCCESS;
						}
						else
						{
							dwRet = ERROR_DEVICE_FEATURE_NOT_SUPPORTED;
						}
					}
				}
				else
				{
					dwRet = ERROR_VC_DISCONNECTED;
				}
			}

			delete conn_info;
		}
		CloseHandle(HubHandle);
	}
	else
	{
		dwRet = GetLastError();
		OOPSERR(dwRet);
	}

	EXIT_FUNCTRET(dwRet);
	return dwRet;
}

BOOL CheckDeviceModem(TCHAR *sHubName, int nPort, int nPid, int nVid)
{
	BOOL bRet = false;
	ENTER_FUNCTION();
	bRet = CheckConnectDevice(sHubName, nPort, nPid, nVid) == ERROR_SUCCESS;
	if (bRet) return bRet;
	else //find Modem( Samsung device Should modem)
	{
		CheckCDROMEject(sHubName, nPort);

		CDeviceStatus ds;
		if (ds.InitDll() == ERROR_SUCCESS)
		{
			BYTE bdata[2048] = { 0 };
			int nLen = 2048;
			DWORD nType = 0;
			if (ds.procGetCommonSymbl(glabel, sHubName, nPort, (PDEVPROPKEY)&DEVPKEY_Device_LocationPaths, bdata, nLen, nType) == ERROR_SUCCESS)
			{
				TCHAR *lcationP = (TCHAR *)bdata;
				TCHAR lcation[MAX_PATH] = { 0 };
				_stprintf_s(lcation, _T("%s#USB(%d)"), lcationP, nPort);
				HDEVINFO h = BeginEnumeratePorts();
				if (h != NULL)
				{
					TCHAR aa[1024] = { 0 };
					EnumeratePortsNext(h, aa, 1024, lcation);
					EndEnumeratePorts(h);

					if (_tcslen(aa) > 0)
					{
						bRet = true;
					}
				}
			}
		}
		else
		{
			logIt(_T("DeviceStatus.dll missing.\n"));
		}
	}

	return bRet;
}

int UninstallUsb(TCHAR *synblink)
{
	ENTER_FUNCTION();
	HANDLE hDevice;
	long	bResult = 0;
	DWORD retu = 0;
	DWORD dwBytesReturned;
	TCHAR szDriv[1024] = { 0 };
	if (synblink == NULL){
		return ERROR_INVALID_PARAMETER;
	}
	_stprintf_s(szDriv, _T("%s"), synblink);
	hDevice = CreateFile(szDriv, GENERIC_WRITE | GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, NULL);
	if (hDevice == INVALID_HANDLE_VALUE){
		logIt(_T("uninstallusb createfile failed error:%d\n"), GetLastError());
		return -1;
	}
	dwBytesReturned = 0;
	PREVENT_MEDIA_REMOVAL PMRBuffer;
	PMRBuffer.PreventMediaRemoval = FALSE;
	if (!DeviceIoControl(hDevice, IOCTL_STORAGE_MEDIA_REMOVAL, &PMRBuffer, sizeof(PREVENT_MEDIA_REMOVAL), NULL, 0, &dwBytesReturned, NULL)){
		logIt(_T("DeviceIoControl IOCTL_STORAGE_MEDIA_REMOVAL failed:%d\n"), GetLastError());
	}
	bResult = DeviceIoControl(hDevice, IOCTL_STORAGE_EJECT_MEDIA, NULL, 0, NULL, 0, &retu, NULL);
	if (!bResult){
		logIt(_T("uninstallusb DeviceIoControl failed error:%d\n"), GetLastError());
		CloseHandle(hDevice);
		return -1;
	}
	CloseHandle(hDevice);
	EXIT_FUNCTRET(0);
	return 0;
}


DWORD EnumsUsbCDROM(TCHAR *multChildren)
{
	DWORD ret = ERROR_UNKNOWN_FEATURE;
	ENTER_FUNCTION();
	GUID* guid = (GUID*)(void*)&GUID_DEVINTERFACE_CDROM; //Samsung is CDROM when phone connect pc
	// Get device interface info set handle for all devices attached to system
	HDEVINFO hDevInfo = SetupDiGetClassDevs(guid, NULL, NULL,
		DIGCF_PRESENT | DIGCF_DEVICEINTERFACE);
	if (hDevInfo == INVALID_HANDLE_VALUE) {
		ret = GetLastError();
		OOPSERR(ret);
		return ret;
	}
	// Retrieve a context structure for a device interface of a device
	// information set.
	
	SP_DEVICE_INTERFACE_DATA devInterfaceData = { 0 };
	devInterfaceData.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);
	BOOL bRet = FALSE;
	TCHAR buffer[1024] = { 0 };
	PSP_DEVICE_INTERFACE_DETAIL_DATA pspdidd = PSP_DEVICE_INTERFACE_DETAIL_DATA(buffer);

	SP_DEVICE_INTERFACE_DATA spdid = { 0 };
	spdid.cbSize = sizeof(spdid);

	SP_DEVINFO_DATA devInfoData = { 0 };
	devInfoData.cbSize = sizeof(SP_DEVINFO_DATA);

	DWORD dwSize = sizeof(buffer)*sizeof(TCHAR);
	for (DWORD dwIndex = 0; SetupDiEnumDeviceInterfaces(hDevInfo, NULL, guid, dwIndex, &devInterfaceData); dwIndex++) {
		SetupDiEnumInterfaceDevice(hDevInfo, NULL, guid, dwIndex, &spdid);
		pspdidd->cbSize = sizeof(*pspdidd);
		dwSize = sizeof(buffer)*sizeof(TCHAR);
		long res = SetupDiGetDeviceInterfaceDetail(hDevInfo, &spdid, pspdidd, dwSize, &dwSize, &devInfoData);
		if (res) {
			DEVPROPTYPE type;
			BYTE b[2048] = { 0 };
			ZeroMemory(b, sizeof(b));
			DWORD sz = sizeof(b);

			if (SetupDiGetDeviceProperty(hDevInfo, &devInfoData, &DEVPKEY_Device_InstanceId, &type, b, sz, &sz, 0))
			{
				TCHAR *instanceid = (TCHAR *)multChildren;
				DWORD dwoffset = 0;
				while (instanceid != NULL && _tcslen(instanceid) > 0)
				{
					if (_tcsicmp((TCHAR*)b, instanceid) == 0)
					{
						logIt(_T("find : %s\n"), pspdidd->DevicePath);
						ret = UninstallUsb(pspdidd->DevicePath);
						break;
					}

					dwoffset += _tcslen(instanceid) + 1;
					instanceid = (TCHAR *)multChildren + dwoffset;
				}
			}
		}		
	}
	SetupDiDestroyDeviceInfoList(hDevInfo);
	EXIT_FUNCTRET(ret);
	return ret;
}

DWORD CheckCDROMEject(TCHAR *sHubName, int nPort)
{
	ENTER_FUNCTION();
	DWORD ret = ERROR_NOT_FOUND;
	//UninstallUsb(_T("M"));
	CDeviceStatus ds;
	if (ds.InitDll() == ERROR_SUCCESS)
	{
		BYTE bdata[2048] = { 0 };
		int nLen = 2048;
		DWORD nType = 0;
		if ((ret = ds.procGetChildPropertyFromHubPort(glabel, sHubName, nPort, (PDEVPROPKEY)&DEVPKEY_Device_InstanceId, bdata, nLen, nType)) == ERROR_SUCCESS)
		{
			logIt(_T("child instanceid %s\n"), (TCHAR*)bdata);
			HDEVINFO hDevInfo = SetupDiCreateDeviceInfoList(NULL, NULL);
			if (hDevInfo != INVALID_HANDLE_VALUE)
			{
				SP_DEVINFO_DATA devInfoData;
				devInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
				if (SetupDiOpenDeviceInfo(hDevInfo, (TCHAR *)bdata, NULL, 0, &devInfoData))
				{
					logIt(_T("device info %d\n"), devInfoData.DevInst);
					DEVPROPTYPE type;
					BYTE b[2048] = { 0 };
					ZeroMemory(b, sizeof(b));
					DWORD sz = sizeof(b);

					if (SetupDiGetDeviceProperty(hDevInfo, &devInfoData, &DEVPKEY_Device_CompatibleIds, &type, b, sz, &sz, 0))
					{
						logIt(_T("device property info %d\n"), devInfoData.DevInst);
						TCHAR *cmpids = (TCHAR *)b;
						DWORD dwoffset = 0;
						while (cmpids != NULL && _tcslen(cmpids)>0)
						{
							logIt(cmpids);			

							if (_tcsicmp(cmpids, _T("USB\\Class_08&SubClass_06")) == 0)
							{
								/*DEVINST devInst;
								ret = CM_Locate_DevNode((PDEVINST)&devInst, (TCHAR *)bdata, CM_LOCATE_DEVNODE_NORMAL);
								logIt(_T("device Node info %d\n"), devInst);
								if (ret != CR_SUCCESS) {
									ret = GetLastError();
									OOPSERR(ret);
									return ret;
								}*/

								ZeroMemory(b, sizeof(b));
								sz = sizeof(b);
								if (SetupDiGetDeviceProperty(hDevInfo, &devInfoData, &DEVPKEY_Device_Children, &type, b, sz, &sz, 0))
								{
									ret = EnumsUsbCDROM((TCHAR *)b);
									Sleep(5000);
									break;
								}
								//why not work ????? 
								//DEVINST devParent = devInfoData.DevInst;
								//ULONG Status = 0;
								//ULONG ProblemNumber = 0;
								//PNP_VETO_TYPE VetoType = PNP_VetoTypeUnknown;
								//ret = CM_Get_DevNode_Status(&Status, &ProblemNumber, devParent, 0);
								//bool IsRemovable = ((Status & DN_REMOVABLE) != 0);
								//for (int tries = 0; tries < 3; tries++)  // sometimes we need some tries...//
								//{
								//	TCHAR errMsg[1024] = { 0 };
								//	if (IsRemovable) {
								//		ret = CM_Request_Device_Eject(devParent, &VetoType, errMsg, sizeof(errMsg), 0); //always return errorcode 23, why??
								//		if (ret == ERROR_SUCCESS)
								//		{
								//			logIt(_T("remove CDROM device successfully.\n"));
								//			Sleep(5000);//wait device remove CDROM and install new driver.
								//			break;
								//		}
								//		logIt(_T("try remove Eject CDROM device. type=%d , %s\n"), VetoType, errMsg);
								//	}
								//	else{
								//		ret = CM_Query_And_Remove_SubTree(devParent, &VetoType, errMsg, sizeof(errMsg), 0); //
								//		if (ret == ERROR_SUCCESS)
								//		{
								//			logIt(_T("remove subTree CDROM device successfully.\n"));
								//			Sleep(5000);//wait device remove CDROM and install new driver.
								//			break;
								//		}
								//		logIt(_T("try remove SubTree CDROM device. type=%d , %s\n"), VetoType, errMsg);
								//	}
								//	Sleep(500);
								//}

								break;
							}
							dwoffset += _tcslen(cmpids) + 1;
							cmpids = (TCHAR *)b + dwoffset;
						}

					}
					else
					{
						ret = GetLastError();
						OOPSERR(ret);
					}
				}
				else
				{
					ret = GetLastError();
					OOPSERR(ret);
				}
			}
			else
			{
				ret = GetLastError();
				OOPSERR(ret);
			}
		}
	}
	EXIT_FUNCTRET(ret);
	return ret;
}

DWORD CheckRefurbishReboot(TCHAR *sHubName, int nPort, int nTimeout)
{
	ENTER_FUNCTION();
	DWORD ret = ERROR_INVALID_PARAMETER;
	TCHAR symblName[1024] = { 0 };
	if (sHubName == NULL || _tcslen(sHubName) == 0 )
		return ERROR_INVALID_PARAMETER;

	if ((_tcsncicmp(sHubName, _T("\\\\?\\"), 4) == 0) || (_tcsncicmp(sHubName, _T("\\\\.\\"), 4) == 0))
	{
		_stprintf_s(symblName, _T("%s"), sHubName);
	}
	else
	{
		_stprintf_s(symblName, _T("%s%s"), _T("\\\\?\\"), sHubName);
	}
	ret = ERROR_TIMEOUT;
	BOOL bRoot = false;
	DWORD dwTimeNow = GetTickCount();
	CString s;
	int cnt = 0;
	extern int gLowTimeOut;
	do
	{
		if (IsNoConnected(symblName, nPort, s))
		{
			logIt(_T("Device disconnected reboot. wait time: %d\n"), cnt++);
			/*if (cnt++ > 2) {
				bRoot = true;
				break;
			}*/
		}
		else
		{
			bRoot = true;
			cnt = 0;
			Sleep(800);
		}
	} while (!bRoot && GetTickCount()-dwTimeNow < gLowTimeOut);

	if (!bRoot)
	{
		logIt(_T("Device not captured reboot.\n"));
		return ERROR_SUCCESS_REBOOT_INITIATED;
	}
	else
	{
		if (gIngoreWaitReboot)
		{
			ret = ERROR_SUCCESS;
			logIt(_T("Device not check reboot back.\n"));
			return ret;
		}
		Sleep(20000);
	}

	dwTimeNow = GetTickCount();
	BOOL bConnected = false;
	do
	{
		if (CheckDeviceModem(symblName, nPort, 0, 0))
		{
			bConnected = true;
			logIt(_T("Device connect again.\n"));
			ret = ERROR_SUCCESS;
			break;
		}
		else
		{
			Sleep(5000);
		}
	} while (!bConnected && GetTickCount() - dwTimeNow < nTimeout);

	EXIT_FUNCTRET(ret);
	return ret;
}
