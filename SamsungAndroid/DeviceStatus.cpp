#include "stdafx.h"
#include "commoncheader.h"
#include "DeviceStatus.h"


CDeviceStatus::CDeviceStatus(void) : fn_GetDeviceStatusHW(NULL),
fn_GetDeviceStatusSymbl(NULL),
fn_GetDeviceLabelPort(NULL),
fn_GetSemaphoreCurCnt(NULL),
fn_GetDeviceInstanceIdFromSymbl(NULL),
m_hDll(NULL)
{
}


CDeviceStatus::~CDeviceStatus(void)
{
	FreeDll();
}

int CDeviceStatus::InitDll()
{
	m_hDll = LoadLibrary(_T("DeviceStatus.dll"));
	if (m_hDll == NULL)
	{
		return GetLastError();
	}
	else
	{
#define resolve_entry(f)	fn_##f = (f)GetProcAddress(m_hDll, #f);\
	if(fn_##f == NULL)\
	{\
		logIt(_T("%S api load failed."), #f);\
	}

		resolve_entry(GetDeviceStatusHW);
		resolve_entry(GetDeviceStatusSymbl);
		resolve_entry(GetDeviceLabelPort);
		resolve_entry(GetSemaphoreCurCnt);
		resolve_entry(GetDeviceInstanceIdFromSymbl);
		resolve_entry(GetDeviceSymblFromInstanceId);
		resolve_entry(GetDeviceSymblFromInstanceIdEx);
		resolve_entry(GetCommonSymbl);
		resolve_entry(GetCommonHW);
		resolve_entry(GetChildPropertyFromHubPort);
#undef	resolve_entry
	}
	return ERROR_SUCCESS;
}
void CDeviceStatus::FreeDll()
{
	if (m_hDll != NULL)
	{
		FreeLibrary(m_hDll);
		m_hDll = NULL;
	}
}
int CDeviceStatus::procGetDeviceStatusHW(int nlabel, LPTSTR sHwid)
{
	int ret = ERROR_PROC_NOT_FOUND;
	if (fn_GetDeviceStatusHW != NULL)
	{
		if (sHwid != NULL && _tcslen(sHwid)>0)
		{
			ret = fn_GetDeviceStatusHW(nlabel, (LPWSTR)sHwid);
		}
		else
			ret = ERROR_INVALID_PARAMETER;
	}
	return ret;
}
int CDeviceStatus::procGetDeviceStatusSymbl(int nlabel, LPTSTR symbl, int nPort)
{
	int ret = ERROR_PROC_NOT_FOUND;
	if (fn_GetDeviceStatusHW != NULL)
	{
		if (symbl != NULL && _tcslen(symbl)>0)
		{
			ret = fn_GetDeviceStatusSymbl(nlabel, (LPWSTR)symbl, nPort);
		}
		else
			ret = ERROR_INVALID_PARAMETER;
	}
	return ret;

}
int CDeviceStatus::procGetDeviceLabelPort(int nlabel, LPTSTR sHwid, LPTSTR sFileName)
{
	int ret = ERROR_PROC_NOT_FOUND;
	if (fn_GetDeviceLabelPort != NULL)
	{
		if ((sHwid != NULL && _tcslen(sHwid)>0) && (sFileName != NULL && _tcslen(sFileName)>0))
		{
			ret = fn_GetDeviceLabelPort(nlabel, (LPWSTR)sHwid, (LPWSTR)sFileName);
		}
		else
			ret = ERROR_INVALID_PARAMETER;
	}
	return ret;

}
int CDeviceStatus::procGetSemaphoreCurCnt(LPTSTR sSemaphoreName)
{
	int ret = ERROR_PROC_NOT_FOUND;
	if (fn_GetSemaphoreCurCnt != NULL)
	{
		if (sSemaphoreName != NULL && _tcslen(sSemaphoreName)>0)
		{
			ret = fn_GetSemaphoreCurCnt((LPWSTR)sSemaphoreName);
		}
		else
			ret = ERROR_INVALID_PARAMETER;
	}
	return ret;

}
int CDeviceStatus::procGetDeviceInstanceIdFromSymbl(
	int nlabel,
	LPCTSTR symbl,
	int nPort,
	LPTSTR lpReturnedString,
	DWORD nSize
	)
{
	int ret = ERROR_PROC_NOT_FOUND;
	ENTER_FUNCTION();
	if (fn_GetDeviceInstanceIdFromSymbl != NULL)
	{
		if (symbl != NULL && _tcslen(symbl)>0)
		{
			logIt(_T("procGetDeviceInstanceIdFromSymbl running \n"));
			ret = fn_GetDeviceInstanceIdFromSymbl(nlabel, (LPWSTR)symbl, nPort, lpReturnedString, nSize);
			if (wcslen(lpReturnedString) > 0)
			{
				logIt(_T("procGetDeviceInstanceIdFromSymbl running result=%d\n"), ret);
			}
		}
		else
			ret = ERROR_INVALID_PARAMETER;
	}
	EXIT_FUNCTRET(ret);
	return ret;

}

int   CDeviceStatus::procGetDeviceSymblFromInstanceId(
	int nlabel,
	LPCTSTR sInstanceId,
	LPTSTR lpReturnedString,
	DWORD nSize
	)
{
	int ret = ERROR_PROC_NOT_FOUND;
	ENTER_FUNCTION();
	if (fn_GetDeviceSymblFromInstanceId != NULL)
	{
		if (sInstanceId != NULL && _tcslen(sInstanceId)>0)
		{
			logIt(_T("procGetDeviceSymblFromInstanceId running \n"));
			ret = fn_GetDeviceSymblFromInstanceId(nlabel, (LPWSTR)sInstanceId, lpReturnedString, nSize);
			if (wcslen(lpReturnedString) > 0)
			{
				logIt(_T("procGetDeviceSymblFromInstanceId running result=%d"), ret);
			}
		}
		else
			ret = ERROR_INVALID_PARAMETER;
	}
	EXIT_FUNCTRET(ret);
	return ret;
}

int CDeviceStatus::procGetCommonSymbl(int nlabel, LPCTSTR sHubName, int nPort, PDEVPROPKEY pdevkey, PBYTE nRet, int &nLen, DWORD &nType)
{
	int ret = ERROR_PROC_NOT_FOUND;
	ENTER_FUNCTION();
	if (fn_GetCommonSymbl != NULL)
	{
		ret = fn_GetCommonSymbl(nlabel, sHubName, nPort, pdevkey, nRet, nLen, nType);
	}
	EXIT_FUNCTRET(ret);
	return ret;
}

int CDeviceStatus::procGetCommonHW(int nlabel, LPCTSTR sHwid, PDEVPROPKEY pdevkey, PBYTE nRet, int &nLen, DWORD &nType)
{
	int ret = ERROR_PROC_NOT_FOUND;
	ENTER_FUNCTION();
	if (fn_GetCommonHW != NULL)
	{
		ret = fn_GetCommonHW(nlabel, sHwid, pdevkey, nRet, nLen, nType);
	}
	EXIT_FUNCTRET(ret);
	return ret;
}

int CDeviceStatus::procGetChildPropertyFromHubPort(
	_In_	 int nlabel,
	_In_     LPCWSTR sHubName,
	_In_	 int nPort,
	_In_	 PDEVPROPKEY pdevkey,
	_Out_    PBYTE nRet,
	_Inout_  int &nLen,
	_Out_    DWORD &nType
	)
{
	int ret = ERROR_PROC_NOT_FOUND;
	ENTER_FUNCTION();
	if (fn_GetChildPropertyFromHubPort != NULL)
	{
		ret = fn_GetChildPropertyFromHubPort(nlabel, sHubName, nPort, pdevkey, nRet, nLen, nType);
	}
	EXIT_FUNCTRET(ret);
	return ret;
}