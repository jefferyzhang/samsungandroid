// SumsungAndroid.cpp : main project file.

#include "stdafx.h"
#include "DeviceStatus.h"
#define INITGUID
#include <initguid.h>
#include <Devpkey.h>
#include <Devpropdef.h>
#include <iostream>
#include <fstream>
#include <set>
#include "commoncheader.h"
using namespace System;
using namespace System::Collections::Specialized;
using namespace System::Configuration::Install;
using namespace System::IO;
using namespace System::Diagnostics;
using namespace System::Text::RegularExpressions;
using namespace System::Web::Script::Serialization;
using namespace System::Collections::Generic;

int glabel = 0;
int gTimeOut = 5;
int gPID = 0;
int gVID = 0;// 0x04E8;
int gLowTimeOut = 30000;
TCHAR JsonFile[MAX_PATH+1] = { 0 };
BOOL gReadLock = true;

void LogIt(System::String^ msg)
{
	System::String^ ss = System::String::Format(L"[Label_{1}]: {0}", msg, glabel);
	System::Diagnostics::Trace::WriteLine(ss);
	//Console::WriteLine(ss);
}

CString LookForBlackList(std::set<CString> vers, CString spModel, CString sMaker) {
	CString sKey;
	if (!PathFileExists(JsonFile)) {
		return _T("");
	}
	String^ json = File::ReadAllText(gcnew String(JsonFile));
	JavaScriptSerializer^ javaScriptSerializer = gcnew JavaScriptSerializer();
	auto jsondata = javaScriptSerializer->Deserialize<Dictionary<String^, Object^>^>(json);
	String^ maker = gcnew String(sMaker);
	if (jsondata->ContainsKey(maker)) {
		for (auto it : vers) {
			sKey.Format(_T("%s_%s"), spModel, it);
			Dictionary<String^, Object^>^ data = (Dictionary<String^, Object^>^)jsondata[maker];
			if (data->ContainsKey(gcnew String(sKey))) {
				if (Convert::ToBoolean(data[gcnew String(sKey)])) {
					return _T("true");
				}
				else {
					return _T("false");
				}
			}
		}
		
	}
	return _T("");
}

void Usage()
{
	Console::WriteLine("command Line:");
	Console::WriteLine("\t -hubname=   connect hub Name.");
	Console::WriteLine("\t -hubport=   connect hub port.");
	Console::WriteLine("\t -info  [-timeout=5  seconds]  device info");
	Console::WriteLine("\t -reactive  Samsung device reactive status info");
	Console::WriteLine("\t -refurbish  -timeout=[5 seconds, now return directly not waiting reboot. no use],  this can use with -info");
	Console::WriteLine("\t -eject  samsung CDROM eject");
	Console::WriteLine("\t -connect  -vid= -pid=    ;hub connect Special Device, if 0 , not check this paramter");
	Console::WriteLine("\t -label=x   ");
	Console::WriteLine("\t -comport=n  this is detect return PORT");
	Console::WriteLine("\t -nofrp  Samsung phone not read LOCK");
	Console::WriteLine("\t -maker=[LG|Samsung] default is SAMSUNG");
}

int RunExeGetInfo(String^ sHubName, int nport)
{
	int ret = ERROR_NOACCESS;
	String^ exepath = Path::Combine(AppDomain::CurrentDomain->BaseDirectory, "WPDUtility.exe");
	if (System::IO::File::Exists(exepath))
	{
		String^ sparam = String::Format("-info -hubname={0} -hubport={1} -label={2}", sHubName, nport, glabel);
		LogIt(String::Format("launch exe with {0}", sparam));
		FileVersionInfo^ vinfo = FileVersionInfo::GetVersionInfo(exepath);
		LogIt(String::Format("{0} version {1}", System::IO::Path::GetFileName(vinfo->FileName), vinfo->FileVersion));
		Process^ p = gcnew Process();
		p->StartInfo->FileName = exepath;
		p->StartInfo->Arguments = sparam;
		p->StartInfo->UseShellExecute = false;
		//p.StartInfo.RedirectStandardError = true;
		p->StartInfo->RedirectStandardOutput = true;
		p->Start();
		StreamReader^ reader = p->StandardOutput;
		String^ result = reader->ReadToEnd();
		Console::WriteLine(result);
		p->WaitForExit();

		ret = p->ExitCode;
	}

	return ret;
}

String^ ConvertIMEIDToIMEI(String^ imeid)
{
	if (imeid->Length != 14) // IMEID must be 15 characters
	{
		return "";
	}

	String^ imei = imeid; // Take the first 14 characters of the IMEID

	 // Initialize all variables
	int imeipart1 = 0;
	int imeipart2 = 0;
	int imeisum = 0;
	int imeichecksum = 0;
	int digit = 0;

	// First block: extract odd values and sum them
	for (int i = 0; i < imei->Length; i++) {
		if (i % 2 == 0) {
			imeipart1 += int::Parse(imei[i].ToString());
		}
	}

	// Second block: extract pair values and shift to match the checksum table
	// 0=>0, 1=>2, 2=>4, 3=>6, 4=>8, 5=>1, 6=>3, 7=>5, 8=>7, 9=>9
	for (int i = 0; i < imei->Length; i++) {
		if (i % 2 == 1) {
			digit = int::Parse(imei[i].ToString());
			if (digit >= 5) {
				digit = ((digit * 2) % 10) + 1;
			}
			else {
				digit = digit * 2;
			}
			imeipart2 += digit;
		}
	}

	// Sum the two results
	imeisum = imeipart1 + imeipart2;

	// The checksum is the difference from the sum to the next round value
	for (int i = 1; (imeisum % 10) != 0; i++) {
		imeisum++;
		imeichecksum = i;
	}

	return imei + imeichecksum.ToString(); // Add the checksum to the end of the IMEI
}

String^ GetCarrierName(String^ carrierCode) {
	if (carrierCode == "ATT")
		return "AT&T";
	else if (carrierCode == "SPR")
		return "Sprint";
	else if (carrierCode == "VZW")
		return "Verizon";
	else if (carrierCode == "TMO" || carrierCode == "TMB" || carrierCode == "TMK")
		return "T-Mobile";
	else if (carrierCode == "USC")
		return "US Cellular";
	else if (carrierCode == "PCT")
		return "Claro";
	else if (carrierCode == "AIO")
		return "Cricket Wireless";
	else if (carrierCode == "BST")
		return "Boost Mobile";
	else if (carrierCode == "VMU")
		return "Virgin Mobile";
	else if (carrierCode == "CCT")
		return "Comcast";
	else if (carrierCode == "null")
		return "Generic Carrier";
	else if (carrierCode == "XAS")
		return "Sprint Sub_Carrier";
	else if (carrierCode == "XAA")
		return "Factory Unlocked";
	else if (carrierCode == "LUC")
		return "LG Uplus - Korea";
	else if (carrierCode == "KTC")
		return "KT Corporation";
	else if (carrierCode == "SKC")
		return "SK Telecom";
	else
		return "";
}

void ParserRegDevconINFO(String^ str, std::map<CString, CString> &info) {
	//str = "AT+DEVCONINFO\r\n+DEVCONINFO: MN(SM-G986U);BASE(UNKNOWN);VER(G986USQS2EVA4/G986UOYN2EVA4/G986USQS2EVA4/G986USQS2EVA4);HIDVER(G986USQS2EVA4/G986UOYN2EVA4/G986USQS2EVA4/G986USQS2EVA4);MNC();MCC();PRD(VZW);AID();CC(US);OMCCODE();SN(R5CN2014E8J);IMEI(35233611870177);UN(CE10193AF8B6297D197E);PN(null);CON(AT,MTP);LOCK(NONE);LIMIT(FALSE);SDP(RUNTIME);HVID(Data:196609,Cache:262145,System:327681)\r\n#OK#\r\n\r\nOK\r\n";
	ENTER_FUNCTION();
	Regex^ reg = gcnew Regex("(?= )((.*?)\\((.*?)\\);)+((.*?)\\((.*?)\\))");
	Match^ mc = reg->Match(str);
	if (mc->Success) {
		String^ data = mc->Groups[0]->Value;
		LogIt(data);
		data = data->Trim();
		array<String ^>^ items = data->Split(';');
		Regex^ kvReg = gcnew Regex("(.*?)\\((.*?)\\)");
		for each(auto item in items) {
			LogIt(item);
			Match^ m = kvReg->Match(item);
			if (m->Success) {
				String^ key = m->Groups[1]->Value;
				String^ value = m->Groups[2]->Value;
				LogIt(String::Format("{0}={1}\n", key, value));

				if (String::Compare(key, "IMEI", true) == 0) {
					if (value->Length == 14) {
						key = "IMEID";
						String^ imeical = ConvertIMEIDToIMEI(value);
						pin_ptr<const wchar_t> wimei = PtrToStringChars(imeical);
						_tprintf(_T("IMEI=%s\n"), wimei);
					}
					else {
						String^ imeiD = value->Substring(0, 14);
						pin_ptr<const wchar_t> wimeiD = PtrToStringChars(imeiD);
						_tprintf(_T("IMEID=%s\n"), wimeiD);
					}
				}

				if (!String::IsNullOrEmpty(value)) {
					pin_ptr<const wchar_t> wkey = PtrToStringChars(key);
					pin_ptr<const wchar_t> wval = PtrToStringChars(value);
					_tprintf(_T("%s=%s\n"), wkey, wval);
					info[wkey] = wval;
					if (String::Compare(key, "PRD", true) == 0) {
						String^ carrier = GetCarrierName(value);
						wval = PtrToStringChars(carrier);
						_tprintf(_T("Carrier=%s\n"), wval);
					}
				}
			}
		}
	}
	
}

void ATIINFOResponse(char* data, std::map<CString, CString> &info) {
	ENTER_FUNCTION();
	String^ str = gcnew String(data);
	array<String^>^ lines = str->Split(gcnew array<Char>{'\r', '\n'}, StringSplitOptions::RemoveEmptyEntries);
	for each (String^ line in lines)
	{
		array<String^>^ lines = line->Split(gcnew array<Char>{':'}, StringSplitOptions::RemoveEmptyEntries);
		if (lines->Length == 2) {
			pin_ptr<const wchar_t> wkey = PtrToStringChars(lines[0]->Trim());
			pin_ptr<const wchar_t> wval = PtrToStringChars(lines[1]->Trim());
			_tprintf(_T("%s=%s\n"), wkey, wval);
			info[wkey] = wval;
		}
	}
	
}

void DevconINFOResponse(char* data, std::map<CString, CString> &info) {
	ENTER_FUNCTION();
	String^ str = gcnew String(data);
	ParserRegDevconINFO(str, info);
}

int main(array<System::String ^> ^args)
{
	//Hub_PlugOff(_T("\\\\?\\USB#ROOT_HUB20#4&291be188&0#{f18a0e88-c30c-11d0-8815-00a0c906bed8}"), 3);
	int exeRet = ERROR_SUCCESS;
	LogIt(String::Format("{0} start: ++ version: {1}",
		Path::GetFileName(Process::GetCurrentProcess()->MainModule->FileName),
		Process::GetCurrentProcess()->MainModule->FileVersionInfo->FileVersion));

	//_sntprintf_s(sIniFileName, 1024, _T("J:\\temp\\calibration.ini"));
	InstallContext^ myInstallContext = gcnew InstallContext(nullptr, args);
	if (myInstallContext->IsParameterTrue("debug"))
	{
		LogIt("Wait any key to dubug, Please attach...");
		System::Console::ReadKey();
	}
	if (myInstallContext->IsParameterTrue("help"))
	{
		Usage();
		System::Console::ReadKey();
	}

	String^ sHubName = String::Empty;
	int nPort = 0;
	TCHAR sHubNameSymb[MAX_PATH] = { 0 };
	if (myInstallContext->Parameters->ContainsKey("hubname"))
	{
		sHubName = myInstallContext->Parameters["hubname"];
		pin_ptr<const TCHAR> content = PtrToStringChars(sHubName);
		_stprintf_s(sHubNameSymb, _T("%s"), content);
	}

	if (true)//myInstallContext->Parameters->ContainsKey("model"))
	{
		String^ apsthome = System::Environment::GetEnvironmentVariable("APSTHOME");
		if (String::IsNullOrEmpty(apsthome))
			apsthome = System::Environment::CurrentDirectory;
		String^ sFileName = Path::Combine(apsthome, "AndroidFile.ini"); //System::AppDomain::CurrentDomain->BaseDirectory
		String^ sConfigFile = Path::Combine(System::AppDomain::CurrentDomain->BaseDirectory, "AdbLessConfig.Json"); //

		extern BOOL gIngoreWaitReboot;
		TCHAR lret[MAX_PATH] = { 0 };
		pin_ptr<const TCHAR> model = PtrToStringChars(myInstallContext->Parameters["model"]);
		pin_ptr<const TCHAR> sfn = PtrToStringChars(sFileName);
		pin_ptr<const TCHAR> sfn1 = PtrToStringChars(sConfigFile);
		_tcscpy_s(JsonFile, sfn1);
		CString s;
		if (myInstallContext->Parameters->ContainsKey("model")) {
			GetPrivateProfileString(_T("ADBlessIgnoreCheckRebootBack"), model, _T(""),
				lret, MAX_PATH, sfn);
			s = lret;
			gIngoreWaitReboot = s.CompareNoCase(_T("true")) == 0;
		}
		if (PathFileExists(JsonFile)) {
			String^ json = File::ReadAllText(sConfigFile);
			JavaScriptSerializer^ javaScriptSerializer = gcnew JavaScriptSerializer();
			auto jsondata = javaScriptSerializer->Deserialize<Dictionary<String^, Object^>^>(json);
			if (jsondata->ContainsKey("ADBlessIgnoreCheckRebootBack")) {
				Dictionary<String^, Object^>^ ignore = (Dictionary<String^, Object^>^)jsondata["ADBlessIgnoreCheckRebootBack"];
				if (ignore->ContainsKey("All")) {
					gIngoreWaitReboot = Convert::ToBoolean(ignore["All"]);
				}
				if (ignore->ContainsKey("lowtimeout")) {
					gLowTimeOut = Convert::ToInt32(ignore["lowtimeout"]);
				}
				if (ignore->ContainsKey("waitdevicegone")) {
					extern BOOL gWaitDeviceGone;
					gWaitDeviceGone = Convert::ToBoolean(ignore["waitdevicegone"]);
				}
			}
		}
		/*GetPrivateProfileString(_T("ADBlessIgnoreCheckRebootBack"), _T("ALL"), _T(""), lret, MAX_PATH, sfn);
		s = lret;
		if (s.GetLength() > 0) {
			if (s.CompareNoCase(_T("true"))==0) {
				gIngoreWaitReboot = true;
			}
			else {
				gIngoreWaitReboot = false;
			}
		}
		gLowTimeOut = GetPrivateProfileInt(_T("ADBlessIgnoreCheckRebootBack"), _T("LowTimeout"), 3000, sfn);*/
	}

	if (myInstallContext->Parameters->ContainsKey("lowtimeout"))
	{
		gLowTimeOut = Convert::ToInt32(myInstallContext->Parameters["lowtimeout"]);
		gLowTimeOut *= 1000; //input Second
	}

	if (myInstallContext->Parameters->ContainsKey("hubport"))
	{
		nPort = Convert::ToInt32(myInstallContext->Parameters["hubport"]);
	}
	if (myInstallContext->Parameters->ContainsKey("label"))
	{
		glabel = Convert::ToInt32(myInstallContext->Parameters["label"]);
	}
	if (myInstallContext->Parameters->ContainsKey("timeout"))
	{
		gTimeOut = Convert::ToInt32(myInstallContext->Parameters["timeout"]);
	}
	if (myInstallContext->Parameters->ContainsKey("pid"))
	{
		gPID = Convert::ToInt32(myInstallContext->Parameters["pid"]);
	}
	if (myInstallContext->Parameters->ContainsKey("vid"))
	{
		gVID = Convert::ToInt32(myInstallContext->Parameters["vid"]);
	}
	if (myInstallContext->Parameters->ContainsKey("comport"))
	{
		extern std::map<CString, CString> m_info;
		int nprt = Convert::ToInt32(myInstallContext->Parameters["comport"]);
		if (nprt > 0)
		{
			CString strComport;
			strComport.Format(_T("COM%d"), nprt);
			m_info[_T("PortName")] = strComport;
		}
	}

	gReadLock = !myInstallContext->IsParameterTrue("nofrp");

	if (myInstallContext->IsParameterTrue("connect"))
	{
		exeRet = CheckConnectDevice(sHubNameSymb, nPort, gPID, gVID);
	}
	if (myInstallContext->IsParameterTrue("eject"))
	{
		exeRet = CheckCDROMEject(sHubNameSymb, nPort);
	}
	BOOL IsLGPhone = false;
	if (myInstallContext->Parameters->ContainsKey("maker")) {
		if (String::Compare("LG", myInstallContext->Parameters["maker"], StringComparison::CurrentCultureIgnoreCase) == 0) {
			IsLGPhone = true;
		}
	}
	if (myInstallContext->IsParameterTrue("info"))
	{
		if (IsLGPhone) {
			exeRet = GetDeviceInfoLG(sHubNameSymb, nPort, TRUE, !myInstallContext->IsParameterTrue("imei"));
			goto EXITPROCESS;
		}

		if (!myInstallContext->IsParameterTrue("imei")){
			RunExeGetInfo(sHubName, nPort);
		}
		exeRet = GetDeviceInfo(sHubNameSymb, nPort, TRUE, !myInstallContext->IsParameterTrue("imei"));
	}
	if (myInstallContext->IsParameterTrue("refurbish"))
	{
		if (IsLGPhone) {
			exeRet = DoRefurbishLG(sHubNameSymb, nPort);
			goto EXITPROCESS;
		}
		exeRet = DoRefurbish(sHubNameSymb, nPort);
	}
	else if (myInstallContext->IsParameterTrue("reactive"))
	{
		exeRet = DoReactivation(sHubNameSymb, nPort);
	}
	else if (myInstallContext->IsParameterTrue("ready"))
	{
		DWORD _start = GetTickCount();
		bool done = false;
		bool ok_IMIE = false;
		bool ok_deviceinfo = false;
		do
		{
			try
			{
				System::Collections::Specialized::StringDictionary^ prop = gcnew System::Collections::Specialized::StringDictionary();
				String^ apsthome = System::Environment::GetEnvironmentVariable("APSTHOME");
				if (String::IsNullOrEmpty(apsthome))
					apsthome = System::Environment::CurrentDirectory;
				apsthome = System::IO::Path::Combine(apsthome, "temp");
				System::IO::Directory::CreateDirectory(apsthome);
				String^ temp_filename = System::IO::Path::Combine(apsthome, String::Format("Label_{0}_ss.txt", glabel));
				if (System::IO::File::Exists(temp_filename))
					System::IO::File::Delete(temp_filename);
				FILE* f = NULL;
				if (_wfreopen_s(&f, (wchar_t*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalUni(temp_filename), L"w", stdout) == NO_ERROR)
				{
					exeRet = GetDeviceInfo(sHubNameSymb, nPort, TRUE, FALSE);
					fclose(f);
				}
				if (exeRet == 0)
				{
					if (System::IO::File::Exists(temp_filename))
					{
						for each (String^ line in System::IO::File::ReadAllLines(temp_filename))
						{
							int pos = line->IndexOf('=');
							String^ k = String::Empty;
							String^ v = String::Empty;
							if (pos > 0)
							{
								k = line->Substring(0, pos);
								v = line->Substring(pos + 1);
							}
							else
							{
								k = line;
							}
							prop[k] = v;
						}
					}
				}
				if (prop->ContainsKey("IMEI"))
				{
					ok_IMIE = true;
				}
				if (prop->ContainsKey("DEVCONINFO"))
				{
					// double check value
					if (String::Compare(prop["DEVCONINFO"], "BUSY", true) == 0)
					{
						// not ready
					}
					else
					{
						ok_deviceinfo = true;
					}
				}
				if (ok_IMIE || ok_deviceinfo)
				{
					done = true;
					exeRet = ERROR_SUCCESS;
				}
				if (!done)
				{
					Sleep(10 * 1000);
					DWORD d = GetTickCount();
					if (d - _start > gTimeOut * 1000)
					{
						done = TRUE;
						exeRet = ERROR_TIMEOUT;
					}
				}
			}
			catch (System::Exception^ e){}
		} while (!done);
	}

EXITPROCESS:
	//format error code.
	LogIt(String::Format("{0} exit orginal error code = {1}", Path::GetFileName(Process::GetCurrentProcess()->MainModule->FileName),exeRet));
	switch (exeRet)
	{
	case ERROR_SUCCESS:
		exeRet = ERROR_SUCCESS;
		break;
	case ERROR_NOT_SUPPORTED:
		exeRet = 21001 + ERROR_NOT_SUPPORTED;
		break;
	case ERROR_DEVICE_FEATURE_NOT_SUPPORTED:
		exeRet = 21001+ ERROR_DEVICE_FEATURE_NOT_SUPPORTED;
		break;
	case ERROR_OPERATION_IN_PROGRESS:
		exeRet = 21001+ ERROR_OPERATION_IN_PROGRESS; //AT not return
		break;
	case ERROR_SUCCESS_REBOOT_INITIATED:
		exeRet = 21001+ ERROR_SUCCESS_REBOOT_INITIATED;   //not find reboot Low
		break;
	case ERROR_TIMEOUT:
		exeRet = 1460;
		break;
	default:
		exeRet = 21001;
		break;
	}

	return exeRet;// == ERROR_SUCCESS ? ERROR_SUCCESS : exeRet + 21001;
}
