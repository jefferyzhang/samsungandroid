#include "stdafx.h"
#include "CliHeader.h"
using namespace System;
using namespace System::Collections::Generic;
using namespace System::Net;
using namespace System::Text;


String^ fetchDataFromUrl(String^ url, String^ username = "", String^ password = "")
{
	String^ ret = "";
	LogIt(String::Format("fetchDataFromUrl: ++ url={0}", url));
	try
	{
		{
			WebClient^ wc = gcnew WebClient();
			System::Net::ServicePointManager::SecurityProtocol = SecurityProtocolType::Tls11 | SecurityProtocolType::Tls12;
			if (!String::IsNullOrEmpty(username) && !String::IsNullOrEmpty(password))
			{
				NetworkCredential^ nc = gcnew NetworkCredential(username, password);
				wc->Credentials = nc;
			}
			array<Byte>^ data = wc->DownloadData(url);
			ret = Encoding::UTF8->GetString(data);
		}
	}
	catch (Exception^ e)
	{
		LogIt(e->Message);
		LogIt(e->StackTrace);
	}
	LogIt(String::Format("fetchDataFromUrl: -- ret={0}", ret));
	return ret;
}

array<Dictionary<String^, Object^>^>^ fetchRecordFromResponseString(String^ s)
{
	array<Dictionary<String^, Object^>^>^ ret = nullptr;
	
	try
	{
		System::Web::Script::Serialization::JavaScriptSerializer^ vss = gcnew System::Web::Script::Serialization::JavaScriptSerializer();
		Dictionary<String^, Object^>^ dict = vss->Deserialize<Dictionary<String^, Object^>^>(s);
		if (dict != nullptr && dict->ContainsKey("_embedded"))
		{
			if (dict["_embedded"]->GetType() == Dictionary<String^, Object^>::typeid)
			{
				Dictionary<String^, Object^>^ d1 = (Dictionary<String^, Object^>^)dict["_embedded"];
				if (d1->ContainsKey("doc") && d1["doc"]->GetType() == System::Collections::ArrayList::typeid)
				{
					System::Collections::ArrayList^ al = (System::Collections::ArrayList^)d1["doc"];
					ret = (array<Dictionary<String^, Object^>^>^)al->ToArray(Dictionary<String^, Object^>::typeid);
				}
			}
		}
	}
	catch (Exception^ e)
	{
		LogIt(e->Message);
		LogIt(e->StackTrace);
	}

	return ret;
}


String^ QueryRecord(String^ sModel, String^ sVer, String^ reactiveret)
{
	String^ ret = "";
	LogIt(String::Format("QueryRecord: ++ model={0}, sver={1}", sModel, sVer));
	String^ uuid = String::Format("Samsung_{0}_{1}", sModel, sVer);
	String^ sUrl = String::Format("http://dc.futuredial.com/cmc/AndroidFRPSupport/\?filter={{\"uuid\":\"{0}\"}}", uuid);
	//sUrl = System::Web::HttpUtility::UrlEncode(sUrl);
	String^ retStr = fetchDataFromUrl(sUrl, "cmc", "cmc1234!");//  http://cmc:cmc1234!@dc.futuredial.com/cmc/testdb/?filter={"uuid":"fdsafds"}
	if (!String::IsNullOrEmpty(retStr))
	{
		array<Dictionary<String^, Object^>^>^ items = fetchRecordFromResponseString(retStr);
		if (items != nullptr)
		{
			if (items[0]->ContainsKey("frpsupport"))
			{
				ret = items[0]["frpsupport"]->ToString();
			}
		}
		else
		{
			ret = "NORECORDITEM";
		}
	}
	else
	{
		ret = "NOCONNECTSERVER";
	}

	LogIt(String::Format("QueryRecord: --"));
	return ret;
}

String^ objectToJsonString(Object^ o)
{
	String^ ret = "";
	if (o->GetType() == Dictionary<String^, Object^>::typeid)
	{
		System::Web::Script::Serialization::JavaScriptSerializer^ jss = gcnew System::Web::Script::Serialization::JavaScriptSerializer();
		ret = jss->Serialize(o);
	}
	return ret;
}

void InsertDB(Dictionary<String^, Object^>^ item)
{
	LogIt(String::Format("InsertDB: ++"));
	if (item == nullptr) return;
	try
	{	
		{
			WebClient^ wc = gcnew WebClient();
			NetworkCredential^ nc = gcnew NetworkCredential("cmc", "cmc1234!");
			wc->Credentials = nc;
			wc->Headers->Add("Content-Type", "application/json");
			wc->Encoding = System::Text::Encoding::UTF8;
			String^ sUrl = ("http://dc.futuredial.com/cmc/AndroidFRPSupport");
			String^ resp = wc->UploadString(sUrl, objectToJsonString(item));
		}
	}
	catch (Exception^ e) { LogIt(e->ToString()); }
	LogIt(String::Format("InsertDB: --"));
}

void InsertDBData(CString model, CString ver)
{
	Dictionary<String^, Object^>^ item = gcnew Dictionary<String^, Object^>();
	String^ s = String::Format("Samsung_{0}_{1}", gcnew String(model), gcnew String(ver));
	item->Add("uuid", s);
	item->Add("maker", "Samsung");
	item->Add("model", gcnew String(model));
	item->Add("version", gcnew String(ver));
	InsertDB(item);
}

int  QueryRecordFrp(CString model, CString ver)
{
	int ret = 0;
	String^ sRet = QueryRecord(gcnew String(model), gcnew String(ver), nullptr);
	if (String::Compare("NO", sRet, true) == 0)
	{
		ret = 0;
	}
	else if (String::Compare("NORECORDITEM", sRet, true) == 0)
	{
		ret = 1;
		InsertDBData(model, ver);
	}
	else
	{
		ret = 2;
	}
	return ret;

}

