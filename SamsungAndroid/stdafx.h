// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include <Windows.h>
#include <tchar.h>
#include <atlbase.h>
#include <atlstr.h>

#include <SetupAPI.h>
#include <Cfgmgr32.h>
#pragma comment(lib, "setupapi.lib")
#pragma comment(lib, "Cfgmgr32.lib")
// TODO: reference additional headers your program requires here

#include <string>
#include <map>
#include <set>
using namespace std;